import React from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import ChatRouter from "./chatsroute";
import Sidepanel from "./containers/Sidepanel";
import Profile from "./containers/Profile";
import AddChatModal from "./containers/Popup";
import * as actions from "./store/actions/auth";
import * as navActions from "./store/actions/nav";
import * as messageActions from "./store/actions/message";
import WebSocketInstance from "./websocket";
import "./assets/chat/css/style.css";
import Chat from "./containers/Chat";
import Hoc from "./hoc/hoc";


class ChatApp2 extends React.Component {

  render() {
    return (


          <div id="frame">
            <Sidepanel />
            <div className="content">
              <AddChatModal
                  isVisible={this.props.showAddChatPopup}
                  close={() => this.props.closeAddChatPopup()}
              />

              <Profile />
<Chat/>

            </div>
          </div>


    );
  }
}
export default ChatApp2