import React, {Component} from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import BaseRouter from "./routes";
import Sidepanel from "./containers/Sidepanel";
import Profile from "./containers/Profile";
import AddChatModal from "./containers/Popup";
import * as actions from "./store/actions/auth";
import * as navActions from "./store/actions/nav";
import * as messageActions from "./store/actions/message";
import WebSocketInstance from "./websocket";
import {createBrowserHistory} from 'history'
import Chat from "./containers/Chat";

import ResponsiveDrawer from "./components/appbar/appbar";
// import "./assets/style.css"
// import 'owl.carousel/dist/assets/owl.carousel.css';
// import 'owl.carousel';
// import 'magnific-popup'
// import 'classy-nav'
// import 'sticky-js'

// import simpleTicker from 'simple-ticker'



class App extends Component {


  constructor(props) {
    super(props);
    WebSocketInstance.addCallbacks(
        this.props.setMessages.bind(this),
        this.props.addMessage.bind(this)
    );
  }

  componentDidMount() {


    this.props.onTryAutoSignup();
  }
history = createBrowserHistory({forceRefresh: true});

  render() {
    return (

        <Router>
          <ResponsiveDrawer >
            <BaseRouter/>
          </ResponsiveDrawer>
        </Router>

    );
  }
}

const mapStateToProps = state => {
  return {
    showAddChatPopup: state.nav.showAddChatPopup,
    authenticated: state.auth.token,
    isAuthenticated: state.auth.token !== null,
    usernamee: state.auth.username

  }
}



const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState()),
    login: (username,password) => dispatch(actions.authLogin(username,password)),
    closeAddChatPopup: () => dispatch(navActions.closeAddChatPopup()),
    addMessage: message => dispatch(messageActions.addMessage(message)),
    setMessages: messages => dispatch(messageActions.setMessages(messages)),
    logout:()=>dispatch(actions.logout())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);