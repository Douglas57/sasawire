import React from "react";
import NavNewsSlide from "./NavNewsSlide";
import SectionMain from "./SectionMain";
import SectionPopular from "./SectionPopular";
import SectionVideo from "./SectionVideo";
import SectionEditors from "./SectionEditors";
import axios from "axios";
import {HOST_URL} from "../settings";
import PostCategory from "../components/post/post/post_categories_single_main";
import image16 from "../assets/img/bg-img/16.jpg";
import PostSmallPoular from "../components/post/post/post_small_poular";
import PostSmallMain from "../components/post/post/post_small_main";


class CategoryContainer extends React.Component {
     state = {
    posts_main:[],
         posts_main_small:[],

         posts_popular_small:[],

    error: null
  };
componentDidMount() {
     this.getAllPostsByType(4);
    this.getAllPostsByType(2);
    let cat=this.props.match.params.category
    this.getAllPostsByCategory(cat);





}

getAllPostsByType(post_type){
            axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        axios.defaults.headers = {
          "Content-Type": "application/json",
  };
        axios.get(`${HOST_URL}/api-v1/posts/type/${post_type}/`, {

          })
          .then(res => {
              if (res.status == 200) {

                  if (post_type === 2) {
                      this.setState({
                          posts_main_small: res.data
                      });
                  }


                  if (post_type === 4) {
                      this.setState({
                          posts_popular_small: res.data
                      });
                  }


                  console.log(res.data)
              }
          })
          .catch(err => {
            console.error(err);
            this.setState({
              error: err
            });
          });



}
getAllPostsByCategory(category){
            axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        axios.defaults.headers = {
          "Content-Type": "application/json",
  };
        axios.get(`${HOST_URL}/api-v1/posts/category/${category}/`, {

          })
          .then(res => {
              if (res.status == 200) {

                      this.setState({
                          posts_main: res.data
                      });



                  console.log(res.data)
              }
          })
          .catch(err => {
            console.error(err);
            this.setState({
              error: err
            });
          });



}
    render() {

    let post_main_list =null;
    let post_main_small_list =null;
    let post_popular_small_list =null;


    if (this.state.posts_main.length >0){
       post_main_list=this.state.posts_main.map(postt => (
            <PostCategory key={postt.slug} post={postt} />
        ));

    }
    if (this.state.posts_main_small.length > 0){
        post_main_small_list=this.state.posts_main_small.map(postt => (
            <PostSmallMain post={postt} />
        ));
    }
    if (this.state.posts_popular_small.length >0){
        post_popular_small_list=this.state.posts_popular_small.map(postt => (
            <PostSmallPoular post={postt} />
        ));
    }

    return (

        <div className="blog-area section-padding-0-80">
            <div className="container">
                <div className="row">
                    <div className="col-12 col-lg-8">
                        <div className="blog-posts-area">
                            {post_main_list}

                        </div>

                        <nav aria-label="Page navigation example">
                            <ul className="pagination mt-50">
                                <li className="page-item active"><a className="page-link" href="#">1</a></li>
                                <li className="page-item"><a className="page-link" href="#">2</a></li>
                                <li className="page-item"><a className="page-link" href="#">3</a></li>
                                <li className="page-item"><a className="page-link" href="#">4</a></li>
                                <li className="page-item"><a className="page-link" href="#">5</a></li>
                                <li className="page-item"><a className="page-link" href="#">...</a></li>
                                <li className="page-item"><a className="page-link" href="#">10</a></li>
                            </ul>
                        </nav>
                    </div>

                    <div className="col-12 col-lg-4">
                        <div className="blog-sidebar-area">


                            <div className="latest-posts-widget mb-50">

                               {post_main_small_list}
                            </div>


                            <div className="popular-news-widget mb-50">
                                <h3>4 Most Popular News</h3>


                                {post_popular_small_list}
                            </div>


                            <div className="newsletter-widget mb-50">
                                <h4>Newsletter</h4>
                                <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                                    mus.</p>
                                <form action="#" method="post">
                                    <input type="text" name="text" placeholder="Name"/>
                                        <input type="email" name="email" placeholder="Email"/>
                                            <button type="submit" className="btn w-100">Subscribe</button>
                                </form>
                            </div>




                    </div>
                </div>
            </div>
        </div>
            </div>
    );
  }
}



export default CategoryContainer;
