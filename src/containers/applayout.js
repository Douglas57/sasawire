import React from "react";
import axios from "axios";
import {HOST_URL} from "../settings";
import LogoImage from "../assets/img/core-img/log.jpeg"
import NavNewsSlide from "./NavNewsSlide";
import * as actions from "../store/actions/auth";
import * as navActions from "../store/actions/nav";
import * as messageActions from "../store/actions/message";
import {connect} from "react-redux";
import WebSocketInstance from "../websocket";
import Categories from "../components/categories/categories";








class Applayout extends React.Component {

    constructor(props) {
        super(props);
    }
    componentDidMount() {


        const { JSDOM } = require( "jsdom" );
        const { window } = new JSDOM( "" );

        const $ = require( "jquery" );
        const jQuery=$

        $(function () {
            var method;
            var noop = function noop() {};
            var methods = [
                'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
                'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
                'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
                'timeStamp', 'trace', 'warn'
            ];
            var length = methods.length;
            var console = (window.console = window.console || {});
            while (length--) {
                method = methods[length];
                // Only stub undefined methods.
                if (!console[method]) {
                    console[method] = noop;
                }
            }
        }());

        $((function(e){"use strict";e.fn.counterUp=function(t){var n=e.extend({time:400,delay:10},t);return this.each(function(){var t=e(this),r=n,i=function(){var e=[],n=r.time/r.delay,i=t.text(),s=/[0-9]+,[0-9]+/.test(i);i=i.replace(/,/g,"");var o=/^[0-9]+$/.test(i),u=/^[0-9]+\.[0-9]+$/.test(i),a=u?(i.split(".")[1]||[]).length:0;for(var f=n;f>=1;f--){var l=parseInt(i/n*f);u&&(l=parseFloat(i/n*f).toFixed(a));if(s)while(/(\d+)(\d{3})/.test(l.toString()))l=l.toString().replace(/(\d+)(\d{3})/,"$1,$2");e.unshift(l)}t.data("counterup-nums",e);t.text("0");var c=function(){t.text(t.data("counterup-nums").shift());if(t.data("counterup-nums").length)setTimeout(t.data("counterup-func"),r.delay);else{delete t.data("counterup-nums");t.data("counterup-nums",null);t.data("counterup-func",null)}};t.data("counterup-func",c);setTimeout(t.data("counterup-func"),r.delay)};t.waypoint(i,{offset:"100%",triggerOnce:!0})})}})
        );

        $(// :: easing js
            jQuery.easing.jswing=jQuery.easing.swing,jQuery.extend(jQuery.easing,{def:"easeOutQuad",swing:function(n,e,t,r,u){return jQuery.easing[jQuery.easing.def](n,e,t,r,u)},easeInQuad:function(n,e,t,r,u){return r*(e/=u)*e+t},easeOutQuad:function(n,e,t,r,u){return-r*(e/=u)*(e-2)+t},easeInOutQuad:function(n,e,t,r,u){return(e/=u/2)<1?r/2*e*e+t:-r/2*(--e*(e-2)-1)+t},easeInCubic:function(n,e,t,r,u){return r*(e/=u)*e*e+t},easeOutCubic:function(n,e,t,r,u){return r*((e=e/u-1)*e*e+1)+t},easeInOutCubic:function(n,e,t,r,u){return(e/=u/2)<1?r/2*e*e*e+t:r/2*((e-=2)*e*e+2)+t},easeInQuart:function(n,e,t,r,u){return r*(e/=u)*e*e*e+t},easeOutQuart:function(n,e,t,r,u){return-r*((e=e/u-1)*e*e*e-1)+t},easeInOutQuart:function(n,e,t,r,u){return(e/=u/2)<1?r/2*e*e*e*e+t:-r/2*((e-=2)*e*e*e-2)+t},easeInQuint:function(n,e,t,r,u){return r*(e/=u)*e*e*e*e+t},easeOutQuint:function(n,e,t,r,u){return r*((e=e/u-1)*e*e*e*e+1)+t},easeInOutQuint:function(n,e,t,r,u){return(e/=u/2)<1?r/2*e*e*e*e*e+t:r/2*((e-=2)*e*e*e*e+2)+t},easeInSine:function(n,e,t,r,u){return-r*Math.cos(e/u*(Math.PI/2))+r+t},easeOutSine:function(n,e,t,r,u){return r*Math.sin(e/u*(Math.PI/2))+t},easeInOutSine:function(n,e,t,r,u){return-r/2*(Math.cos(Math.PI*e/u)-1)+t},easeInExpo:function(n,e,t,r,u){return 0==e?t:r*Math.pow(2,10*(e/u-1))+t},easeOutExpo:function(n,e,t,r,u){return e==u?t+r:r*(-Math.pow(2,-10*e/u)+1)+t},easeInOutExpo:function(n,e,t,r,u){return 0==e?t:e==u?t+r:(e/=u/2)<1?r/2*Math.pow(2,10*(e-1))+t:r/2*(-Math.pow(2,-10*--e)+2)+t},easeInCirc:function(n,e,t,r,u){return-r*(Math.sqrt(1-(e/=u)*e)-1)+t},easeOutCirc:function(n,e,t,r,u){return r*Math.sqrt(1-(e=e/u-1)*e)+t},easeInOutCirc:function(n,e,t,r,u){return(e/=u/2)<1?-r/2*(Math.sqrt(1-e*e)-1)+t:r/2*(Math.sqrt(1-(e-=2)*e)+1)+t},easeInElastic:function(n,e,t,r,u){var a=1.70158,i=0,s=r;if(0==e)return t;if(1==(e/=u))return t+r;if(i||(i=.3*u),s<Math.abs(r)){s=r;var a=i/4}else var a=i/(2*Math.PI)*Math.asin(r/s);return-(s*Math.pow(2,10*(e-=1))*Math.sin((e*u-a)*(2*Math.PI)/i))+t},easeOutElastic:function(n,e,t,r,u){var a=1.70158,i=0,s=r;if(0==e)return t;if(1==(e/=u))return t+r;if(i||(i=.3*u),s<Math.abs(r)){s=r;var a=i/4}else var a=i/(2*Math.PI)*Math.asin(r/s);return s*Math.pow(2,-10*e)*Math.sin((e*u-a)*(2*Math.PI)/i)+r+t},easeInOutElastic:function(n,e,t,r,u){var a=1.70158,i=0,s=r;if(0==e)return t;if(2==(e/=u/2))return t+r;if(i||(i=u*(.3*1.5)),s<Math.abs(r)){s=r;var a=i/4}else var a=i/(2*Math.PI)*Math.asin(r/s);return 1>e?-.5*(s*Math.pow(2,10*(e-=1))*Math.sin((e*u-a)*(2*Math.PI)/i))+t:s*Math.pow(2,-10*(e-=1))*Math.sin((e*u-a)*(2*Math.PI)/i)*.5+r+t},easeInBack:function(n,e,t,r,u,a){return void 0==a&&(a=1.70158),r*(e/=u)*e*((a+1)*e-a)+t},easeOutBack:function(n,e,t,r,u,a){return void 0==a&&(a=1.70158),r*((e=e/u-1)*e*((a+1)*e+a)+1)+t},easeInOutBack:function(n,e,t,r,u,a){return void 0==a&&(a=1.70158),(e/=u/2)<1?r/2*(e*e*(((a*=1.525)+1)*e-a))+t:r/2*((e-=2)*e*(((a*=1.525)+1)*e+a)+2)+t},easeInBounce:function(n,e,t,r,u){return r-jQuery.easing.easeOutBounce(n,u-e,0,r,u)+t},easeOutBounce:function(n,e,t,r,u){return(e/=u)<1/2.75?r*(7.5625*e*e)+t:2/2.75>e?r*(7.5625*(e-=1.5/2.75)*e+.75)+t:2.5/2.75>e?r*(7.5625*(e-=2.25/2.75)*e+.9375)+t:r*(7.5625*(e-=2.625/2.75)*e+.984375)+t},easeInOutBounce:function(n,e,t,r,u){return u/2>e?.5*jQuery.easing.easeInBounce(n,2*e,0,r,u)+t:.5*jQuery.easing.easeOutBounce(n,2*e-u,0,r,u)+.5*r+t}})
        );

// eslint-disable-next-line no-unused-expressions
        $(function(l,o,e){"use strict";l.fn.scrollUp=function(o){l.data(e.body,"scrollUp")||(l.data(e.body,"scrollUp",!0),l.fn.scrollUp.init(o))},l.fn.scrollUp.init=function(r){var s,t,c,i,n,a,d,p=l.fn.scrollUp.settings=l.extend({},l.fn.scrollUp.defaults,r),f=!1;switch(d=p.scrollTrigger?l(p.scrollTrigger):l("<a/>",{id:p.scrollName,href:"#top"}),p.scrollTitle&&d.attr("title",p.scrollTitle),d.appendTo("body"),p.scrollImg||p.scrollTrigger||d.html(p.scrollText),d.css({display:"none",position:"fixed",zIndex:p.zIndex}),p.activeOverlay&&l("<div/>",{id:p.scrollName+"-active"}).css({position:"absolute",top:p.scrollDistance+"px",width:"100%",borderTop:"1px dotted"+p.activeOverlay,zIndex:p.zIndex}).appendTo("body"),p.animation){case"fade":s="fadeIn",t="fadeOut",c=p.animationSpeed;break;case"slide":s="slideDown",t="slideUp",c=p.animationSpeed;break;default:s="show",t="hide",c=0}i="top"===p.scrollFrom?p.scrollDistance:l(e).height()-l(o).height()-p.scrollDistance,n=l(o).scroll(function(){l(o).scrollTop()>i?f||(d[s](c),f=!0):f&&(d[t](c),f=!1)}),p.scrollTarget?"number"==typeof p.scrollTarget?a=p.scrollTarget:"string"==typeof p.scrollTarget&&(a=Math.floor(l(p.scrollTarget).offset().top)):a=0,d.click(function(o){o.preventDefault(),l("html, body").animate({scrollTop:a},p.scrollSpeed,p.easingType)})},l.fn.scrollUp.defaults={scrollName:"scrollUp",scrollDistance:300,scrollFrom:"top",scrollSpeed:300,easingType:"linear",animation:"fade",animationSpeed:200,scrollTrigger:!1,scrollTarget:!1,scrollText:"Scroll to top",scrollTitle:!1,scrollImg:!1,activeOverlay:!1,zIndex:2147483647},l.fn.scrollUp.destroy=function(r){l.removeData(e.body,"scrollUp"),l("#"+l.fn.scrollUp.settings.scrollName).remove(),l("#"+l.fn.scrollUp.settings.scrollName+"-active").remove(),l.fn.jquery.split(".")[1]>=7?l(o).off("scroll",r):l(o).unbind("scroll",r)},l.scrollUp=l.fn.scrollUp}(jQuery,window,document)
        );


// eslint-disable-next-line no-unused-expressions
        (function(){var a,b,c,d,e,f=function(a,b){return function(){return a.apply(b,arguments)}},g=[].indexOf||function(a){for(var b=0,c=this.length;c>b;b++)if(b in this&&this[b]===a)return b;return-1};b=function(){function a(){}return a.prototype.extend=function(a,b){var c,d;for(c in b)d=b[c],null==a[c]&&(a[c]=d);return a},a.prototype.isMobile=function(a){return/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(a)},a.prototype.createEvent=function(a,b,c,d){var e;return null==b&&(b=!1),null==c&&(c=!1),null==d&&(d=null),null!=document.createEvent?(e=document.createEvent("CustomEvent"),e.initCustomEvent(a,b,c,d)):null!=document.createEventObject?(e=document.createEventObject(),e.eventType=a):e.eventName=a,e},a.prototype.emitEvent=function(a,b){return null!=a.dispatchEvent?a.dispatchEvent(b):b in(null!=a)?a[b]():"on"+b in(null!=a)?a["on"+b]():void 0},a.prototype.addEvent=function(a,b,c){return null!=a.addEventListener?a.addEventListener(b,c,!1):null!=a.attachEvent?a.attachEvent("on"+b,c):a[b]=c},a.prototype.removeEvent=function(a,b,c){return null!=a.removeEventListener?a.removeEventListener(b,c,!1):null!=a.detachEvent?a.detachEvent("on"+b,c):delete a[b]},a.prototype.innerHeight=function(){return"innerHeight"in window?window.innerHeight:document.documentElement.clientHeight},a}(),c=this.WeakMap||this.MozWeakMap||(c=function(){function a(){this.keys=[],this.values=[]}return a.prototype.get=function(a){var b,c,d,e,f;for(f=this.keys,b=d=0,e=f.length;e>d;b=++d)if(c=f[b],c===a)return this.values[b]},a.prototype.set=function(a,b){var c,d,e,f,g;for(g=this.keys,c=e=0,f=g.length;f>e;c=++e)if(d=g[c],d===a)return void(this.values[c]=b);return this.keys.push(a),this.values.push(b)},a}()),a=this.MutationObserver||this.WebkitMutationObserver||this.MozMutationObserver||(a=function(){function a(){"undefined"!=typeof console&&null!==console&&console.warn("MutationObserver is not supported by your browser."),"undefined"!=typeof console&&null!==console&&console.warn("WOW.js cannot detect dom mutations, please call .sync() after loading new content.")}return a.notSupported=!0,a.prototype.observe=function(){},a}()),d=this.getComputedStyle||function(a,b){return this.getPropertyValue=function(b){var c;return"float"===b&&(b="styleFloat"),e.test(b)&&b.replace(e,function(a,b){return b.toUpperCase()}),(null!=(c=a.currentStyle)?c[b]:void 0)||null},this},e=/(\-([a-z]){1})/g,this.WOW=function(){function e(a){null==a&&(a={}),this.scrollCallback=f(this.scrollCallback,this),this.scrollHandler=f(this.scrollHandler,this),this.resetAnimation=f(this.resetAnimation,this),this.start=f(this.start,this),this.scrolled=!0,this.config=this.util().extend(a,this.defaults),null!=a.scrollContainer&&(this.config.scrollContainer=document.querySelector(a.scrollContainer)),this.animationNameCache=new c,this.wowEvent=this.util().createEvent(this.config.boxClass)}return e.prototype.defaults={boxClass:"wow",animateClass:"animated",offset:0,mobile:!0,live:!0,callback:null,scrollContainer:null},e.prototype.init=function(){var a;return this.element=window.document.documentElement,"interactive"===(a=document.readyState)||"complete"===a?this.start():this.util().addEvent(document,"DOMContentLoaded",this.start),this.finished=[]},e.prototype.start=function(){var b,c,d,e;if(this.stopped=!1,this.boxes=function(){var a,c,d,e;for(d=this.element.querySelectorAll("."+this.config.boxClass),e=[],a=0,c=d.length;c>a;a++)b=d[a],e.push(b);return e}.call(this),this.all=function(){var a,c,d,e;for(d=this.boxes,e=[],a=0,c=d.length;c>a;a++)b=d[a],e.push(b);return e}.call(this),this.boxes.length)if(this.disabled())this.resetStyle();else for(e=this.boxes,c=0,d=e.length;d>c;c++)b=e[c],this.applyStyle(b,!0);return this.disabled()||(this.util().addEvent(this.config.scrollContainer||window,"scroll",this.scrollHandler),this.util().addEvent(window,"resize",this.scrollHandler),this.interval=setInterval(this.scrollCallback,50)),this.config.live?new a(function(a){return function(b){var c,d,e,f,g;for(g=[],c=0,d=b.length;d>c;c++)f=b[c],g.push(function(){var a,b,c,d;for(c=f.addedNodes||[],d=[],a=0,b=c.length;b>a;a++)e=c[a],d.push(this.doSync(e));return d}.call(a));return g}}(this)).observe(document.body,{childList:!0,subtree:!0}):void 0},e.prototype.stop=function(){return this.stopped=!0,this.util().removeEvent(this.config.scrollContainer||window,"scroll",this.scrollHandler),this.util().removeEvent(window,"resize",this.scrollHandler),null!=this.interval?clearInterval(this.interval):void 0},e.prototype.sync=function(b){return a.notSupported?this.doSync(this.element):void 0},e.prototype.doSync=function(a){var b,c,d,e,f;if(null==a&&(a=this.element),1===a.nodeType){for(a=a.parentNode||a,e=a.querySelectorAll("."+this.config.boxClass),f=[],c=0,d=e.length;d>c;c++)b=e[c],g.call(this.all,b)<0?(this.boxes.push(b),this.all.push(b),this.stopped||this.disabled()?this.resetStyle():this.applyStyle(b,!0),f.push(this.scrolled=!0)):f.push(void 0);return f}},e.prototype.show=function(a){return this.applyStyle(a),a.className=a.className+" "+this.config.animateClass,null!=this.config.callback&&this.config.callback(a),this.util().emitEvent(a,this.wowEvent),this.util().addEvent(a,"animationend",this.resetAnimation),this.util().addEvent(a,"oanimationend",this.resetAnimation),this.util().addEvent(a,"webkitAnimationEnd",this.resetAnimation),this.util().addEvent(a,"MSAnimationEnd",this.resetAnimation),a},e.prototype.applyStyle=function(a,b){var c,d,e;return d=a.getAttribute("data-wow-duration"),c=a.getAttribute("data-wow-delay"),e=a.getAttribute("data-wow-iteration"),this.animate(function(f){return function(){return f.customStyle(a,b,d,c,e)}}(this))},e.prototype.animate=function(){return"requestAnimationFrame"in window?function(a){return window.requestAnimationFrame(a)}:function(a){return a()}}(),e.prototype.resetStyle=function(){var a,b,c,d,e;for(d=this.boxes,e=[],b=0,c=d.length;c>b;b++)a=d[b],e.push(a.style.visibility="visible");return e},e.prototype.resetAnimation=function(a){var b;return a.type.toLowerCase().indexOf("animationend")>=0?(b=a.target||a.srcElement,b.className=b.className.replace(this.config.animateClass,"").trim()):void 0},e.prototype.customStyle=function(a,b,c,d,e){return b&&this.cacheAnimationName(a),a.style.visibility=b?"hidden":"visible",c&&this.vendorSet(a.style,{animationDuration:c}),d&&this.vendorSet(a.style,{animationDelay:d}),e&&this.vendorSet(a.style,{animationIterationCount:e}),this.vendorSet(a.style,{animationName:b?"none":this.cachedAnimationName(a)}),a},e.prototype.vendors=["moz","webkit"],e.prototype.vendorSet=function(a,b){var c,d,e,f;d=[];for(c in b)e=b[c],a[""+c]=e,d.push(function(){var b,d,g,h;for(g=this.vendors,h=[],b=0,d=g.length;d>b;b++)f=g[b],h.push(a[""+f+c.charAt(0).toUpperCase()+c.substr(1)]=e);return h}.call(this));return d},e.prototype.vendorCSS=function(a,b){var c,e,f,g,h,i;for(h=d(a),g=h.getPropertyCSSValue(b),f=this.vendors,c=0,e=f.length;e>c;c++)i=f[c],g=g||h.getPropertyCSSValue("-"+i+"-"+b);return g},e.prototype.animationName=function(a){var b;try{b=this.vendorCSS(a,"animation-name").cssText}catch(c){b=d(a).getPropertyValue("animation-name")}return"none"===b?"":b},e.prototype.cacheAnimationName=function(a){return this.animationNameCache.set(a,this.animationName(a))},e.prototype.cachedAnimationName=function(a){return this.animationNameCache.get(a)},e.prototype.scrollHandler=function(){return this.scrolled=!0},e.prototype.scrollCallback=function(){var a;return!this.scrolled||(this.scrolled=!1,this.boxes=function(){var b,c,d,e;for(d=this.boxes,e=[],b=0,c=d.length;c>b;b++)a=d[b],a&&(this.isVisible(a)?this.show(a):e.push(a));return e}.call(this),this.boxes.length||this.config.live)?void 0:this.stop()},e.prototype.offsetTop=function(a){for(var b;void 0===a.offsetTop;)a=a.parentNode;for(b=a.offsetTop;a=a.offsetParent;)b+=a.offsetTop;return b},e.prototype.isVisible=function(a){var b,c,d,e,f;return c=a.getAttribute("data-wow-offset")||this.config.offset,f=this.config.scrollContainer&&this.config.scrollContainer.scrollTop||window.pageYOffset,e=f+Math.min(this.element.clientHeight,this.util().innerHeight())-c,d=this.offsetTop(a),b=d+a.clientHeight,e>=d&&b>=f},e.prototype.util=function(){return null!=this._util?this._util:this._util=new b},e.prototype.disabled=function(){return!this.config.mobile&&this.util().isMobile(navigator.userAgent)},e}()}).call(this);


// eslint-disable-next-line no-unused-expressions
        $(function(e){e.simpleTicker=(t,i)=>{var s={speed:1e3,delay:3e3,easing:"swing",effectType:"slide"},n={ul:"",li:"",initList:"",ulWidth:"",liHeight:"",tickerHook:"tickerHook",effect:{}},c=this;c.settings={};e(t),t=t;c.init=function(){switch(c.settings=e.extend({},s,i),n.ul=t.children("ul"),n.li=t.find("li"),n.initList=t.find("li:first"),n.ulWidth=n.ul.width(),n.liHeight=n.li.height(),t.css({height:n.liHeight}),n.li.css({top:"0",left:"0",position:"absolute"}),c.settings.effectType){case"fade":c.effect.fade();break;case"roll":c.effect.roll();break;case"slide":c.effect.slide()}c.effect.exec()},c.effect={},c.effect.exec=function(){n.initList.css(n.effect.init.css).animate(n.effect.init.animate,c.settings.speed,c.settings.easing).addClass(n.tickerHook),t.find(n.li).length>1&&setInterval(function(){t.find("."+n.tickerHook).animate(n.effect.start.animate,c.settings.speed,c.settings.easing).next().css(n.effect.next.css).animate(n.effect.next.animate,c.settings.speed,c.settings.easing).addClass(n.tickerHook).end().appendTo(n.ul).css(n.effect.end.css).removeClass(n.tickerHook)},c.settings.delay)},c.effect.fade=function(){n.effect={init:{css:{display:"block",opacity:"0"},animate:{opacity:"1",zIndex:"98"}},start:{animate:{opacity:"0"}},next:{css:{display:"block",opacity:"0",zIndex:"99"},animate:{opacity:"1"}},end:{css:{display:"none",zIndex:"98"}}}},c.effect.roll=function(){n.effect={init:{css:{top:"3em",display:"block",opacity:"0"},animate:{top:"0",opacity:"1",zIndex:"98"}},start:{animate:{top:"-3em",opacity:"0"}},next:{css:{top:"3em",display:"block",opacity:"0",zIndex:"99"},animate:{top:"0",opacity:"1"}},end:{css:{zIndex:"98"}}}},c.effect.slide=function(){n.effect={init:{css:{left:200,display:"block",opacity:"0"},animate:{left:"0",opacity:"1",zIndex:"98"}},start:{animate:{left:-200,opacity:"0"}},next:{css:{left:n.ulWidth,display:"block",opacity:"0",zIndex:"99"},animate:{left:"0",opacity:"1"}},end:{css:{zIndex:"98"}}}},c.init()},e.fn.simpleTicker=function(t){return this.each(function(){if(void 0==e(this).data("simpleTicker")){var i=new e.simpleTiecker(this,t);e(this).data("simpleTicker",i)}})}});

// eslint-disable-next-line no-unused-expressions
        $(function(e){e.fn.niceSelect=function(t){function s(t){t.after(e("<div></div>").addClass("nice-select").addClass(t.attr("class")||"").addClass(t.attr("disabled")?"disabled":"").attr("tabindex",t.attr("disabled")?null:"0").html('<span class="current"></span><ul class="list"></ul>'));var s=t.next(),n=t.find("option"),i=t.find("option:selected");s.find(".current").html(i.data("display")||i.text()),n.each(function(t){var n=e(this),i=n.data("display");s.find("ul").append(e("<li></li>").attr("data-value",n.val()).attr("data-display",i||null).addClass("option"+(n.is(":selected")?" selected":"")+(n.is(":disabled")?" disabled":"")).html(n.text()))})}if("string"==typeof t)return"update"==t?this.each(function(){var t=e(this),n=e(this).next(".nice-select"),i=n.hasClass("open");n.length&&(n.remove(),s(t),i&&t.next().trigger("click"))}):"destroy"==t?(this.each(function(){var t=e(this),s=e(this).next(".nice-select");s.length&&(s.remove(),t.css("display",""))}),0==e(".nice-select").length&&e(document).off(".nice_select")):console.log('Method "'+t+'" does not exist.'),this;this.hide(),this.each(function(){var t=e(this);t.next().hasClass("nice-select")||s(t)}),e(document).off(".nice_select"),e(document).on("click.nice_select",".nice-select",function(t){var s=e(this);e(".nice-select").not(s).removeClass("open"),s.toggleClass("open"),s.hasClass("open")?(s.find(".option"),s.find(".focus").removeClass("focus"),s.find(".selected").addClass("focus")):s.focus()}),e(document).on("click.nice_select",function(t){0===e(t.target).closest(".nice-select").length&&e(".nice-select").removeClass("open").find(".option")}),e(document).on("click.nice_select",".nice-select .option:not(.disabled)",function(t){var s=e(this),n=s.closest(".nice-select");n.find(".selected").removeClass("selected"),s.addClass("selected");var i=s.data("display")||s.text();n.find(".current").text(i),n.prev("select").val(s.data("value")).trigger("change")}),e(document).on("keydown.nice_select",".nice-select",function(t){var s=e(this),n=e(s.find(".focus")||s.find(".list .option.selected"));if(32==t.keyCode||13==t.keyCode)return s.hasClass("open")?n.trigger("click"):s.trigger("click"),!1;if(40==t.keyCode){if(s.hasClass("open")){var i=n.nextAll(".option:not(.disabled)").first();i.length>0&&(s.find(".focus").removeClass("focus"),i.addClass("focus"))}else s.trigger("click");return!1}if(38==t.keyCode){if(s.hasClass("open")){var l=n.prevAll(".option:not(.disabled)").first();l.length>0&&(s.find(".focus").removeClass("focus"),l.addClass("focus"))}else s.trigger("click");return!1}if(27==t.keyCode)s.hasClass("open")&&s.trigger("click");else if(9==t.keyCode&&s.hasClass("open"))return!1});var n=document.createElement("a").style;return n.cssText="pointer-events:auto","auto"!==n.pointerEvents&&e("html").addClass("no-csspointerevents"),this}});
// eslint-disable-next-line no-unused-expressions
        $(function(){var t=function(t){this.context=t.getContext("2d"),this.refElement=t.parentNode,this.loaded=0,this.start=4.72,this.width=this.context.canvas.width,this.height=this.context.canvas.height,this.total=parseInt(this.refElement.dataset.percent,10),this.timer=null,this.diff=0,this.init()};t.prototype={init:function(){var t=this;t.timer=setInterval(function(){t.run()},25)},run:function(){var t=this;t.diff=(t.loaded/100*Math.PI*2*10).toFixed(2),t.context.clearRect(0,0,t.width,t.height),t.context.lineWidth=5,t.context.fillStyle="#000",t.context.strokeStyle="#40ba37",t.context.textAlign="center",t.context.fillText(t.loaded+"%",.5*t.width,.5*t.height+2,t.width),t.context.beginPath(),t.context.arc(35,35,30,t.start,t.diff/10+t.start,!1),t.context.stroke(),t.loaded>=t.total&&clearInterval(t.timer),t.loaded++}};var e=function(t){this.bars=document.querySelectorAll(t),this.bars.length>0&&this.init()};e.prototype={init:function(){this.tick=25,this.progress()},progress:function(){var e=this,n=0,i=e.bars[0].querySelector("canvas"),r=(new t(i),setInterval(function(){n++;var i=e.bars[n].querySelector("canvas");new t(i);n==e.bars.length&&clearInterval(r)},100*e.tick))}},document.addEventListener("DOMContentLoaded",function(){new e(".single-pie-bar")})}()
        );
// eslint-disable-next-line no-unused-expressions
        $(function(e){e.fn.classyNav=function(n){var s=e(".classy-nav-container"),a=e(".classynav ul"),o=e(".classynav > ul > li"),l=e(".classy-navbar-toggler"),i=e(".classycloseIcon"),t=e(".navbarToggler"),d=e(".classy-menu"),r=e(window),c=e.extend({theme:"light",breakpoint:991,openCloseSpeed:350,megaopenCloseSpeed:700,alwaysHidden:!1,openMobileMenu:"left",dropdownRtl:!1,stickyNav:!1,stickyFooterNav:!1},n);return this.each(function(){function n(){window.innerWidth<=c.breakpoint?s.removeClass("breakpoint-off").addClass("breakpoint-on"):s.removeClass("breakpoint-on").addClass("breakpoint-off")}"light"!==c.theme&&"dark"!==c.theme||s.addClass(c.theme),"left"!==c.openMobileMenu&&"right"!==c.openMobileMenu||s.addClass(c.openMobileMenu),!0===c.dropdownRtl&&s.addClass("dropdown-rtl"),l.on("click",function(){t.toggleClass("active"),d.toggleClass("menu-on")}),i.on("click",function(){d.removeClass("menu-on"),t.removeClass("active")}),o.has(".dropdown").addClass("cn-dropdown-item"),o.has(".megamenu").addClass("megamenu-item"),a.find("li a").each(function(){e(this).next().length>0&&e(this).parent("li").addClass("has-down").append('<span class="dd-trigger"></span>')}),a.find("li .dd-trigger").on("click",function(n){n.preventDefault(),e(this).parent("li").children("ul").stop(!0,!0).slideToggle(c.openCloseSpeed),e(this).parent("li").toggleClass("active")}),e(".megamenu-item").removeClass("has-down"),a.find("li .dd-trigger").on("click",function(n){n.preventDefault(),e(this).parent("li").children(".megamenu").slideToggle(c.megaopenCloseSpeed)}),n(),r.on("resize",function(){n()}),!0===c.alwaysHidden&&s.addClass("breakpoint-on").removeClass("breakpoint-off"),!0===c.stickyNav&&r.on("scroll",function(){r.scrollTop()>0?s.addClass("classy-sticky"):s.removeClass("classy-sticky")}),!0===c.stickyFooterNav&&s.addClass("classy-sticky-footer")})}}
        );



        $(function ($) {


            var browserWindow = $(window);

            // :: 1.0 Preloader Active Code
            browserWindow.on('load', function () {
                $('#preloader').fadeOut('slow', function () {
                    $(this).remove();
                });
            });

            // :: 2.0 Newsticker Active Code
            $.simpleTicker($("#breakingNewsTicker"), {
                speed: 1000,
                delay: 3000,
                easing: 'swing',
                effectType: 'roll'
            });
            $.simpleTicker($("#internationalTicker"), {
                speed: 1000,
                delay: 4000,
                easing: 'swing',
                effectType: 'roll'
            });

            // :: 3.0 Nav Active Code
            if ($.fn.classyNav) {
                $('#newspaperNav').classyNav();
            }

            // :: 4.0 Gallery Active Code
            if ($.fn.magnificPopup) {
                $('.videoPlayer').magnificPopup({
                    type: 'iframe'
                });
            }

            // :: 5.0 ScrollUp Active Code
            if ($.fn.scrollUp) {
                browserWindow.scrollUp({
                    scrollSpeed: 1500,
                    scrollText: '<i class="fa fa-angle-up"></i>'
                });
            }

            // :: 6.0 CouterUp Active Code
            if ($.fn.counterUp) {
                $('.counter').counterUp({
                    delay: 10,
                    time: 2000
                });
            }

            // :: 7.0 Sticky Active Code
            if ($.fn.sticky) {
                $("#stickyMenu").sticky({
                    topSpacing: 0
                });
            }

            // :: 8.0 wow Active Code
            if (browserWindow.width() > 767) {
                //new WOW().init();
            }

            // :: 9.0 prevent default a click
            $('a[href="#"]').click(function ($) {
                $.preventDefault()
            });

        });


    }










    render() {



        return (
            <div>
                <header className="header-area">


                    <div className="top-header-area">
                        <div className="container">
                            <div className="row">
                                <div className="col-12">
                                    <div
                                        className="top-header-content d-flex align-items-center justify-content-between">

                                        <div className="logo">
                                            <a href=""><img src={LogoImage} alt=""/></a>
                                        </div>


                                        <div className="login-search-area d-flex align-items-center">
                                            {this.props.isAuthenticated
                                                ?
                                                (
                                                    <div className="login d-flex">
                                                        <a href="#"
                                                           onClick={this.props.logout}>Logout {this.props.usernamee}</a>

                                                    </div>
                                                )

                                                :
                                                (
                                                    <div className="login d-flex">
                                                        <a href={'/login/'}>Login</a>
                                                        <a href={'/register/'}>Register</a>
                                                    </div>
                                                )
                                            }


                                            <div className="search-form">
                                                <form action="#" method="post">
                                                    <input type="search" name="search" className="form-control"
                                                           placeholder="Search"/>
                                                    <button type="submit">
                                                        <i className="fa fa-search" aria-hidden="true"></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="newspaper-main-menu" id="stickyMenu">
                        <div className="classy-nav-container breakpoint-off">
                            <div className="container">

                                <nav className="classy-navbar justify-content-between" id="newspaperNav">


                                    <div className="logo">
                                        <a href="#"><img src={LogoImage} alt=""/></a>
                                    </div>


                                    <div className="classy-navbar-toggler">
                                        <span className="navbarToggler"><span></span><span></span><span></span></span>
                                    </div>


                                    <div className="classy-menu">


                                        <div className="classycloseIcon">
                                            <div className="cross-wrap"><span className="top"></span><span
                                                className="bottom"></span></div>
                                        </div>

                                        {cats}


                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </header>
                <NavNewsSlide/>
                {this.props.children}


                <footer className="footer-area">


                    <div className="main-footer-area">
                        <div className="container">
                            <div className="row">


                                <div className="col-12 col-sm-6 col-lg-4">
                                    <div className="footer-widget-area mt-80">

                                        <div className="footer-logo">
                                            <a href={``}><img src={LogoImage} alt=""/></a>
                                        </div>

                                        <ul className="list">
                                            <li><a href="mailto:contact@youremail.com">contact@youremail.com</a></li>
                                            <li><a href="tel:+4352782883884">+43 5278 2883 884</a></li>
                                            <li><a href="http://yoursitename.com">www.yoursitename.com</a></li>
                                        </ul>
                                    </div>
                                </div>


                                <div className="col-12 col-sm-6 col-lg-2">
                                    <div className="footer-widget-area mt-80">

                                        <h4 className="widget-title">Politics</h4>

                                        <ul className="list">
                                            <li><a href="#">Business</a></li>
                                            <li><a href="#">Markets</a></li>
                                            <li><a href="#">Tech</a></li>
                                            <li><a href="#">Luxury</a></li>
                                        </ul>
                                    </div>
                                </div>


                                <div className="col-12 col-sm-4 col-lg-2">
                                    <div className="footer-widget-area mt-80">

                                        <h4 className="widget-title">Featured</h4>

                                        <ul className="list">
                                            <li><a href="#">Football</a></li>
                                            <li><a href="#">Golf</a></li>
                                            <li><a href="#">Tennis</a></li>
                                            <li><a href="#">Motorsport</a></li>
                                            <li><a href="#">Horseracing</a></li>
                                            <li><a href="#">Equestrian</a></li>
                                            <li><a href="#">Sailing</a></li>
                                            <li><a href="#">Skiing</a></li>
                                        </ul>
                                    </div>
                                </div>


                                <div className="col-12 col-sm-4 col-lg-2">
                                    <div className="footer-widget-area mt-80">

                                        <h4 className="widget-title">FAQ</h4>

                                        <ul className="list">
                                            <li><a href="#">Aviation</a></li>
                                            <li><a href="#">Business</a></li>
                                            <li><a href="#">Traveller</a></li>
                                            <li><a href="#">Destinations</a></li>
                                            <li><a href="#">Features</a></li>
                                            <li><a href="#">Food/Drink</a></li>
                                            <li><a href="#">Hotels</a></li>
                                            <li><a href="#">Partner Hotels</a></li>
                                        </ul>
                                    </div>
                                </div>


                                <div className="col-12 col-sm-4 col-lg-2">
                                    <div className="footer-widget-area mt-80">

                                        <h4 className="widget-title">+More</h4>

                                        <ul className="list">
                                            <li><a href="#">Fashion</a></li>
                                            <li><a href="#">Design</a></li>
                                            <li><a href="#">Architecture</a></li>
                                            <li><a href="#">Arts</a></li>
                                            <li><a href="#">Autos</a></li>
                                            <li><a href="#">Luxury</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="bottom-footer-area">
                        <div className="container h-100">
                            <div className="row h-100 align-items-center">
                                <div className="col-12">

                                    <p>
                                        Copyright &copy;
                                        <script>document.write(new Date().getFullYear());</script>
                                        All rights reserved | This template is made with <i className="fa fa-heart-o"
                                                                                            aria-hidden="true"></i> by <a
                                        href="https://colorlib.com" target="_blank">Colorlib</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {

        authenticated: state.auth.token,
        isAuthenticated: state.auth.token !== null,
        usernamee: state.auth.username

    }
}

const mapDispatchToProps = dispatch => {
    return {
        onTryAutoSignup: () => dispatch(actions.authCheckState()),
        logout:()=>dispatch(actions.logout())
    };
};

export default connect(mapStateToProps, mapDispatchToProps) (BaseLayout);
