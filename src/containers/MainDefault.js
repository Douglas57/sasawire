import React from "react";
import axios from "axios";
import {HOST_URL} from "../settings";
import LogoImage from "../assets/img/core-img/log.jpeg"
import NavNewsSlide from "./NavNewsSlide";
import * as actions from "../store/actions/auth";
import * as navActions from "../store/actions/nav";
import * as messageActions from "../store/actions/message";
import {connect} from "react-redux";
import WebSocketInstance from "../websocket";


import ResponsiveDrawer from "../components/appbar/appbar";









class BaseLayout extends React.Component {
  state = {
    categories:[],
    error: null
  };
    constructor(props) {
        super(props);
    }
componentDidMount() {





    this.getAllCategories()
    // this.props.onTryAutoSignup();
}


getAllCategories(){
            axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        axios.defaults.headers = {
          "Content-Type": "application/json",
  };
        axios.get(`${HOST_URL}/api-v1/categories/`, {

          })
          .then(res => {
            this.setState({
              categories: res.data
            });
            console.log(this.state.categories)


          })
          .catch(err => {
            console.error(err);
            this.setState({
              error: err
            });


          });



}




    render() {
let appbar=null
    if(this.state.categories.length>0) {

        appbar= (<ResponsiveDrawer propschildren={propsChild} newsNav={newsNav} footer={footer}
                             categories={this.state.categories}/>)

    }
let footer=(                <footer className="footer-area">


    <div className="main-footer-area">
        <div className="container">
            <div className="row">


                <div className="col-12 col-sm-6 col-lg-4">
                    <div className="footer-widget-area mt-80">

                        <div className="footer-logo">
                            <a href={``}><img src={LogoImage} alt=""/></a>
                        </div>

                        <ul className="list">
                            <li><a href="mailto:contact@youremail.com">contact@youremail.com</a></li>
                            <li><a href="tel:+4352782883884">+43 5278 2883 884</a></li>
                            <li><a href="http://yoursitename.com">www.yoursitename.com</a></li>
                        </ul>
                    </div>
                </div>


                <div className="col-12 col-sm-6 col-lg-2">
                    <div className="footer-widget-area mt-80">

                        <h4 className="widget-title">Politics</h4>

                        <ul className="list">
                            <li><a href="#">Business</a></li>
                            <li><a href="#">Markets</a></li>
                            <li><a href="#">Tech</a></li>
                            <li><a href="#">Luxury</a></li>
                        </ul>
                    </div>
                </div>


                <div className="col-12 col-sm-4 col-lg-2">
                    <div className="footer-widget-area mt-80">

                        <h4 className="widget-title">Featured</h4>

                        <ul className="list">
                            <li><a href="#">Football</a></li>
                            <li><a href="#">Golf</a></li>
                            <li><a href="#">Tennis</a></li>
                            <li><a href="#">Motorsport</a></li>
                            <li><a href="#">Horseracing</a></li>
                            <li><a href="#">Equestrian</a></li>
                            <li><a href="#">Sailing</a></li>
                            <li><a href="#">Skiing</a></li>
                        </ul>
                    </div>
                </div>


                <div className="col-12 col-sm-4 col-lg-2">
                    <div className="footer-widget-area mt-80">

                        <h4 className="widget-title">FAQ</h4>

                        <ul className="list">
                            <li><a href="#">Aviation</a></li>
                            <li><a href="#">Business</a></li>
                            <li><a href="#">Traveller</a></li>
                            <li><a href="#">Destinations</a></li>
                            <li><a href="#">Features</a></li>
                            <li><a href="#">Food/Drink</a></li>
                            <li><a href="#">Hotels</a></li>
                            <li><a href="#">Partner Hotels</a></li>
                        </ul>
                    </div>
                </div>


                <div className="col-12 col-sm-4 col-lg-2">
                    <div className="footer-widget-area mt-80">

                        <h4 className="widget-title">+More</h4>

                        <ul className="list">
                            <li><a href="#">Fashion</a></li>
                            <li><a href="#">Design</a></li>
                            <li><a href="#">Architecture</a></li>
                            <li><a href="#">Arts</a></li>
                            <li><a href="#">Autos</a></li>
                            <li><a href="#">Luxury</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div className="bottom-footer-area">
        <div className="container h-100">
            <div className="row h-100 align-items-center">
                <div className="col-12">

                    <p>
                        Copyright &copy;
                        <script>document.write(new Date().getFullYear());</script>
                        All rights reserved | This template is made with <i className="fa fa-heart-o"
                                                                            aria-hidden="true"></i> by <a
                        href="https://colorlib.com" target="_blank">Colorlib</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>)
        let newsNav=<NavNewsSlide/>
        let propsChild=this.props.children

        return (
            <div>
{/*                <header className="header-area">*/}


{/*                    <div className="top-header-area">*/}
{/*                        <div className="container">*/}
{/*                            <div className="row">*/}
{/*                                <div className="col-12">*/}
{/*                                    <div*/}
{/*                                        className="top-header-content d-flex align-items-center justify-content-between">*/}

{/*                                        <div className="logo">*/}
{/*                                            <a href=""><img src={LogoImage} alt=""/></a>*/}
{/*                                        </div>*/}


{/*                                        <div className="login-search-area d-flex align-items-center">*/}
{/*{this.props.isAuthenticated*/}
{/*    ?*/}
{/*    (*/}
{/*    <div className="login d-flex">*/}
{/*        <a href="#"*/}
{/*        onClick={this.props.logout}>Logout {this.props.usernamee}</a>*/}

{/*    </div>*/}
{/*    )*/}

{/*    :*/}
{/*    (*/}
{/*    <div className="login d-flex">*/}
{/*        <a href={'/login/'}>Login</a>*/}
{/*        <a href={'/register/'}>Register</a>*/}
{/*    </div>*/}
{/*    )*/}
{/*    }*/}


{/*                                            <div className="search-form">*/}
{/*                                                <form action="#" method="post">*/}
{/*                                                    <input type="search" name="search" className="form-control"*/}
{/*                                                           placeholder="Search"/>*/}
{/*                                                    <button type="submit">*/}
{/*                                                        <i className="fa fa-search" aria-hidden="true"></i>*/}
{/*                                                    </button>*/}
{/*                                                </form>*/}
{/*                                            </div>*/}
{/*                                        </div>*/}
{/*                                    </div>*/}
{/*                                </div>*/}
{/*                            </div>*/}
{/*                        </div>*/}
{/*                    </div>*/}
{/*                </header>*/}



{/*<NavNewsSlide/>*/}
{/*                {this.props.children}*/}


{/*                <footer className="footer-area">*/}


{/*                    <div className="main-footer-area">*/}
{/*                        <div className="container">*/}
{/*                            <div className="row">*/}


{/*                                <div className="col-12 col-sm-6 col-lg-4">*/}
{/*                                    <div className="footer-widget-area mt-80">*/}

{/*                                        <div className="footer-logo">*/}
{/*                                            <a href={``}><img src={LogoImage} alt=""/></a>*/}
{/*                                        </div>*/}

{/*                                        <ul className="list">*/}
{/*                                            <li><a href="mailto:contact@youremail.com">contact@youremail.com</a></li>*/}
{/*                                            <li><a href="tel:+4352782883884">+43 5278 2883 884</a></li>*/}
{/*                                            <li><a href="http://yoursitename.com">www.yoursitename.com</a></li>*/}
{/*                                        </ul>*/}
{/*                                    </div>*/}
{/*                                </div>*/}


{/*                                <div className="col-12 col-sm-6 col-lg-2">*/}
{/*                                    <div className="footer-widget-area mt-80">*/}

{/*                                        <h4 className="widget-title">Politics</h4>*/}

{/*                                        <ul className="list">*/}
{/*                                            <li><a href="#">Business</a></li>*/}
{/*                                            <li><a href="#">Markets</a></li>*/}
{/*                                            <li><a href="#">Tech</a></li>*/}
{/*                                            <li><a href="#">Luxury</a></li>*/}
{/*                                        </ul>*/}
{/*                                    </div>*/}
{/*                                </div>*/}


{/*                                <div className="col-12 col-sm-4 col-lg-2">*/}
{/*                                    <div className="footer-widget-area mt-80">*/}

{/*                                        <h4 className="widget-title">Featured</h4>*/}

{/*                                        <ul className="list">*/}
{/*                                            <li><a href="#">Football</a></li>*/}
{/*                                            <li><a href="#">Golf</a></li>*/}
{/*                                            <li><a href="#">Tennis</a></li>*/}
{/*                                            <li><a href="#">Motorsport</a></li>*/}
{/*                                            <li><a href="#">Horseracing</a></li>*/}
{/*                                            <li><a href="#">Equestrian</a></li>*/}
{/*                                            <li><a href="#">Sailing</a></li>*/}
{/*                                            <li><a href="#">Skiing</a></li>*/}
{/*                                        </ul>*/}
{/*                                    </div>*/}
{/*                                </div>*/}


{/*                                <div className="col-12 col-sm-4 col-lg-2">*/}
{/*                                    <div className="footer-widget-area mt-80">*/}

{/*                                        <h4 className="widget-title">FAQ</h4>*/}

{/*                                        <ul className="list">*/}
{/*                                            <li><a href="#">Aviation</a></li>*/}
{/*                                            <li><a href="#">Business</a></li>*/}
{/*                                            <li><a href="#">Traveller</a></li>*/}
{/*                                            <li><a href="#">Destinations</a></li>*/}
{/*                                            <li><a href="#">Features</a></li>*/}
{/*                                            <li><a href="#">Food/Drink</a></li>*/}
{/*                                            <li><a href="#">Hotels</a></li>*/}
{/*                                            <li><a href="#">Partner Hotels</a></li>*/}
{/*                                        </ul>*/}
{/*                                    </div>*/}
{/*                                </div>*/}


{/*                                <div className="col-12 col-sm-4 col-lg-2">*/}
{/*                                    <div className="footer-widget-area mt-80">*/}

{/*                                        <h4 className="widget-title">+More</h4>*/}

{/*                                        <ul className="list">*/}
{/*                                            <li><a href="#">Fashion</a></li>*/}
{/*                                            <li><a href="#">Design</a></li>*/}
{/*                                            <li><a href="#">Architecture</a></li>*/}
{/*                                            <li><a href="#">Arts</a></li>*/}
{/*                                            <li><a href="#">Autos</a></li>*/}
{/*                                            <li><a href="#">Luxury</a></li>*/}
{/*                                        </ul>*/}
{/*                                    </div>*/}
{/*                                </div>*/}
{/*                            </div>*/}
{/*                        </div>*/}
{/*                    </div>*/}

                {appbar}
{/*                    <div className="bottom-footer-area">*/}
{/*                        <div className="container h-100">*/}
{/*                            <div className="row h-100 align-items-center">*/}
{/*                                <div className="col-12">*/}

{/*                                    <p>*/}
{/*                                        Copyright &copy;*/}
{/*                                        <script>document.write(new Date().getFullYear());</script>*/}
{/*                                        All rights reserved | This template is made with <i className="fa fa-heart-o"*/}
{/*                                                                                            aria-hidden="true"></i> by <a*/}
{/*                                        href="https://colorlib.com" target="_blank">Colorlib</a>*/}
{/*                                    </p>*/}
{/*                                </div>*/}
{/*                            </div>*/}
{/*                        </div>*/}
{/*                    </div>*/}
{/*                </footer>*/}

            </div>

        );
    }
}

const mapStateToProps = state => {
    return {

        authenticated: state.auth.token,
        isAuthenticated: state.auth.token !== null,
        usernamee: state.auth.username

    }
}

const mapDispatchToProps = dispatch => {
    return {
        onTryAutoSignup: () => dispatch(actions.authCheckState()),
        logout:()=>dispatch(actions.logout())
    };
};

export default BaseLayout;
