import React from "react";

import image16 from "../assets/img/bg-img/16.jpg"
//import "../assets/style.css"
import backImage from "../assets/img/bg-img/bg1.jpg"




const SectionVideo= (props)=>{
    return (
        // eslint-disable-next-line react/style-prop-object
        <div className="video-post-area bg-img bg-overlay" style={{ background: backImage }}>
            <div className="container">
                <div className="row justify-content-center">

                    <div className="col-12 col-sm-6 col-md-4">
                        <div className="single-video-post">
                            <img src={image16} alt=""/>

                                <div className="videobtn">
                                    <a href="https://www.youtube.com/watch?v=5BQr-j3BBzU" className="videoPlayer"><i
                                        className="fa fa-play" aria-hidden="true"></i></a>
                                </div>
                        </div>
                    </div>


                    <div className="col-12 col-sm-6 col-md-4">
                        <div className="single-video-post">
                            <img src={image16} alt=""/>

                                <div className="videobtn">
                                    <a href="https://www.youtube.com/watch?v=5BQr-j3BBzU" className="videoPlayer"><i
                                        className="fa fa-play" aria-hidden="true"></i></a>
                                </div>
                        </div>
                    </div>


                    <div className="col-12 col-sm-6 col-md-4">
                        <div className="single-video-post">
                           <img src={image16} alt=""/>

                                <div className="videobtn">
                                    <a href="https://www.youtube.com/watch?v=5BQr-j3BBzU" className="videoPlayer"><i
                                        className="fa fa-play" aria-hidden="true"></i></a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
  }




export default SectionVideo;
