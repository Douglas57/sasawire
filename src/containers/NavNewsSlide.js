import React from "react";

import adgif from "../assets/img/bg-img/hero-add.gif"
import Slide from '@material-ui/core/Slide';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
const useStyles = makeStyles((theme) => ({
    root: {
        height: 180,
    },
    wrapper: {
        width: 100 + theme.spacing(2),
    },
    paper: {
        zIndex: 1,
        position: 'relative',
        margin: theme.spacing(1),
    },
    svg: {
        width: 100,
        height: 100,
    },
    polygon: {
        fill: theme.palette.common.white,
        stroke: theme.palette.divider,
        strokeWidth: 1,
    },
}));






const NavNewsSlide= (props)=>{
    const classes = useStyles();
    const [currentBreaking,setCurrentBreaking]=React.useState(null)
    const [currentInternational,setCurrentInternational]=React.useState(null)

    React.useEffect(()=>{
        let theBreakingList=props.breaking

        let cuur=0
            setInterval(()=>{

                if (cuur===theBreakingList.length){
                    cuur=0
                }

                setCurrentBreaking([theBreakingList[cuur].title,theBreakingList[cuur].slug])
                cuur =cuur+1

            },2000)


    },[])
    React.useEffect(()=>{
        let theInternationalList=props.international

        let cuur=0
        setInterval(()=>{

            if (cuur===theInternationalList.length){
                cuur=0
            }

            setCurrentInternational([theInternationalList[cuur].title,theInternationalList[cuur].slug])
            cuur =cuur+1

        },2000)


    },[])
    return (
        <div className="hero-area">
            <div className="container">
                <div className="row align-items-center">

                    <div className="col-12 col-lg-8">
                        {currentBreaking===null?'':(
                        <div className="breaking-news-area d-flex align-items-center">
                            <div className="news-title">
                                <p>Breaking News</p>
                            </div>
                            <div id="breakingNewsTicker" className="ticker">
                                <Slide direction="up" in={true} mountOnEnter unmountOnExit>
                                    <a href={`/posts/view/${currentBreaking[1]}/`}>
                                    <Paper elevation={1} className={classes.paper}>
                                        {currentBreaking[0]}
                                    </Paper>
                                        </a>
                                </Slide>
                            </div>
                        </div>

                            )}
                            {currentInternational===null?'':(
                        <div className="breaking-news-area d-flex align-items-center mt-15">
                            <div className="news-title title2">
                                <p>International</p>
                            </div>
                            <div id="internationalTicker" className="ticker">
                                <Slide direction="up" in={true} mountOnEnter unmountOnExit>
                                    <a href={`/posts/view/${currentInternational[1]}/`}>
                                    <Paper elevation={1} className={classes.paper}>
                                        {currentInternational[0]}
                                    </Paper>
                                    </a>
                                </Slide>
                            </div>
                        </div>
                                )}
                    </div>


                    <div className="col-12 col-lg-4">
                        <div className="hero-add">
                            <a href="#"><img src={adgif} alt=""/></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
  }




export default NavNewsSlide;
