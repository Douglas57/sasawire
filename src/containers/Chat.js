import React from "react";
import { connect } from "react-redux";
import WebSocketInstance from "../websocket";
import Hoc from "../hoc/hoc";
import Sidepanel from "./Sidepanel";
import AddChatModal from "./Popup";
import Profile from "./Profile";
import * as actions from "../store/actions/auth";
import * as navActions from "../store/actions/nav";
import * as messageActions from "../store/actions/message";

class Chat extends React.Component {
  state = { message: "" };

  initialiseChat() {
     WebSocketInstance.connect(this.props.chatId);
    this.waitForSocketConnection(() => {
      WebSocketInstance.fetchMessages(
          this.props.username,
          this.props.chatId
      );
    });


  }

  constructor(props) {
    super(props);


    WebSocketInstance.addCallbacks(
        this.props.setMessages.bind(this),
        this.props.addMessage.bind(this)
    );
this.initialiseChat()
  }

  waitForSocketConnection(callback) {
    const component = this;
    setTimeout(function() {
      if (WebSocketInstance.state() === 1) {
        console.log("Connection is made");
        callback();
        return;
      } else {
        console.log("wait for connection...");
        component.waitForSocketConnection(callback);
      }
    }, 100);
  }

  messageChangeHandler = event => {
    this.setState({ message: event.target.value });
  };

  sendMessageHandler = e => {
    e.preventDefault();
    const messageObject = {
      from: this.props.username,
      content: this.state.message,
      chatId: this.props.chatId
    };
    WebSocketInstance.newChatMessage(messageObject);
    this.setState({ message: "" });
  };

  renderTimestamp = timestamp => {
    let prefix = "";
    const timeDiff = Math.round(
        (new Date().getTime() - new Date(timestamp).getTime()) / 60000
    );
    if (timeDiff < 1) {
      // less than one minute ago
      prefix = "just now...";
    } else if (timeDiff < 60 && timeDiff > 1) {
      // less than sixty minutes ago
      prefix = `${timeDiff} minutes ago`;
    } else if (timeDiff < 24 * 60 && timeDiff > 60) {
      // less than 24 hours ago
      prefix = `${Math.round(timeDiff / 60)} hours ago`;
    } else if (timeDiff < 31 * 24 * 60 && timeDiff > 24 * 60) {
      // less than 7 days ago
      prefix = `${Math.round(timeDiff / (60 * 24))} days ago`;
    } else {
      prefix = `${new Date(timestamp)}`;
    }
    return prefix;
  };

  renderMessages = messages => {
    const currentUser = this.props.username;
    return messages.map((message, i, arr) => (
        <li
            key={message.id}
            style={{ marginBottom: arr.length - 1 === i ? "300px" : "15px" }}
            className={message.author === currentUser ? "sent" : "replies"}
        >
          <img
              src="http://emilcarlsson.se/assets/mikeross.png"
              alt="profile-pic"
          />
          <p>
            {message.content}
            <br />
            <small>{this.renderTimestamp(message.timestamp)}</small>
          </p>
        </li>
    ));
  };

  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: "smooth" });
  };

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  componentWillReceiveProps(newProps) {
    if (this.props.chatId!== newProps.chatId) {
      WebSocketInstance.disconnect();
      this.waitForSocketConnection(() => {
        WebSocketInstance.fetchMessages(
            this.props.username,
            newProps.chatId
        );
      });
      WebSocketInstance.connect(newProps.chatId);
    }
  }

  render() {
    return (
        <div>

          <div className="messages">
            <ul id="chat-log">
              {this.props.messages && this.renderMessages(this.props.messages)}
              <div
                  style={{ float: "left", clear: "both" }}
                  ref={el => {
                    this.messagesEnd = el;
                  }}
              />
            </ul>
          </div>
          <div className="message-input">
            <form onSubmit={this.sendMessageHandler}>
              <div className="wrap">
                <input
                    onChange={this.messageChangeHandler}
                    value={this.state.message}
                    required
                    id="chat-message-input"
                    type="text"
                    placeholder="Write your message..."
                />
                <i className="fa fa-paperclip attachment" aria-hidden="true" />
                <button id="chat-message-submit" className="submit">
                  <i className="fa fa-paper-plane" aria-hidden="true" />
                </button>
              </div>
            </form>
          </div>
        </div>

    );
  }
}

const mapStateToProps = state => {
  return {
    username: state.auth.username,
    messages: state.message.messages,
    showAddChatPopup: state.nav.showAddChatPopup,
    authenticated: state.auth.token
  };
};



const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState()),
    closeAddChatPopup: () => dispatch(navActions.closeAddChatPopup()),
    addMessage: message => dispatch(messageActions.addMessage(message)),
    setMessages: messages => dispatch(messageActions.setMessages(messages))
  };
};
export default connect(mapStateToProps,mapDispatchToProps)(Chat);
