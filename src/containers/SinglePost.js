import React from "react";

import axios from "axios";
import {HOST_URL} from "../settings";
import PostCategory from "../components/post/post/post_categories_single_main";
import PostSmallPoular from "../components/post/post/post_small_poular";
import PostSmallMain from "../components/post/post/post_small_main";
import CommentContainer from "./CommentContainer";

class SinglePostContainer extends React.Component {
     state = {
    postObject:null,
         posts_main_small:[],

         posts_popular_small:[],
         all_comments:[],

    error: null
  };
componentDidMount() {
     this.getAllPostsByType(4);
    this.getAllPostsByType(2);
    let slug=this.props.match.params.slug;
    this.getAllPostsBySlug(slug)





}

getAllPostsByType(post_type){
            axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        axios.defaults.headers = {
          "Content-Type": "application/json",
  };
        axios.get(`${HOST_URL}/api-v1/posts/type/${post_type}/`, {

          })
          .then(res => {
              if (res.status == 200) {

                  if (post_type === 2) {
                      this.setState({
                          posts_main_small: res.data
                      });
                  }


                  if (post_type === 4) {
                      this.setState({
                          posts_popular_small: res.data
                      });
                  }


                  console.log(res.data)
              }
          })
          .catch(err => {
            console.error(err);
            this.setState({
              error: err
            });
          });



}
getAllPostsBySlug(slug){
            axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        axios.defaults.headers = {
          "Content-Type": "application/json",
  };
        axios.get(`${HOST_URL}/api-v1/posts/view/${slug}/`, {

          })
          .then(res => {
              if (res.status == 200) {

                      this.setState({
                          postObject: res.data
                      });



                  console.log(res.data)
              }
          })
          .catch(err => {
            console.error(err);
            this.setState({
              error: err
            });
          });



}


    render() {

    let post_main_list =null;
    let commentCont =null;
    let post_main_small_list =null;
    let post_popular_small_list =null;
    let total_comments=0
    if (this.state.postObject !== null){
       post_main_list=(<PostCategory post={this.state.postObject} />);
       if(this.state.postObject.is_commentable){
       commentCont=(<CommentContainer total_comments={this.state.postObject.total_comments} slug={this.state.postObject.slug}
       post_id={this.state.postObject.id}/>);
       }

    }
    if (this.state.posts_main_small.length > 0){
        post_main_small_list=this.state.posts_main_small.map(postt => (
            <PostSmallMain post={postt} />
        ));
    }
    if (this.state.posts_popular_small.length >0){
        post_popular_small_list=this.state.posts_popular_small.map(postt => (
            <PostSmallPoular post={postt} />
        ));
    }

    return (

        <div className="blog-area section-padding-0-80">
            <div className="container">
                <div className="row">
                    <div className="col-12 col-lg-8">
                        <div className="blog-posts-area">
                            {post_main_list}
                            {commentCont}

                        </div>



                    </div>

                    <div className="col-12 col-lg-4">
                        <div className="blog-sidebar-area">


                            <div className="latest-posts-widget mb-50">

                               {post_main_small_list}
                            </div>


                            <div className="popular-news-widget mb-50">
                                <h3>4 Most Popular News</h3>


                                {post_popular_small_list}
                            </div>


                            <div className="newsletter-widget mb-50">
                                <h4>Newsletter</h4>
                                <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                                    mus.</p>
                                <form action="#" method="post">
                                    <input type="text" name="text" placeholder="Name"/>
                                        <input type="email" name="email" placeholder="Email"/>
                                            <button type="submit" className="btn w-100">Subscribe</button>
                                </form>
                            </div>




                    </div>
                </div>
            </div>
        </div>
            </div>
    );
  }
}



export default SinglePostContainer;
