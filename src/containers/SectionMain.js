import React from "react";

import image16 from "../assets/img/bg-img/16.jpg"
//import "../assets/style.css"
import PostMain from "../components/post/post/post_main";
import PostMain2 from "../components/post/post/post_main2";
import PostSmallMain from "../components/post/post/post_small_main";




const SectionMain= (props)=>{
    let postmain =null
    let postmain2 =null
    let postmain3 =null
    let postsmallsmain =null
    if (props.posts.length >0){
       postmain= (<PostMain post={props.posts[0]} image={image16}/>)
    }
        if (props.posts.length >1){
        postmain2= (<PostMain2 post={props.posts[1]} image={image16}/>)}
    if (props.posts.length >2){
        postmain3= (<PostMain2 post={props.posts[2]} image={image16}/>)

    }
    if (props.posts_small.length >0){
        postsmallsmain=props.posts_small.map(postt => (
            <PostSmallMain post={postt} image={image16}/>
        ));
    }


    return (
    <div className="featured-post-area">
        <div className="container">
            <div className="row">
                <div className="col-12 col-md-6 col-lg-8">
                    <div className="row">


                        <div className="col-12 col-lg-7">
                           {postmain}

                        </div>

                        <div className="col-12 col-lg-5">

                            {postmain2}


                            {postmain3}
                        </div>
                    </div>
                </div>

                <div className="col-12 col-md-6 col-lg-4">

                    {postsmallsmain}
                </div>
            </div>
        </div>
    </div>
    );
  }




export default SectionMain;
