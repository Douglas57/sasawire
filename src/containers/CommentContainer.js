import React from "react";

//import image16 from "../assets/img/bg-img/16.jpg"
//import "../assets/style.css"
import Comment from "../components/comment/comment";
//import default_image from "../assets/img/default.jpg";
import axios from "axios";
import {HOST_URL} from "../settings";
import CommentTextArea from "../components/comment/CommentTextArea";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";

import TreeView from "@material-ui/lab/TreeView";
import {fade, makeStyles} from "@material-ui/core/styles";
import CollapsibleComment from "../components/comment/collapsible_comment";
import {deepOrange} from "@material-ui/core/colors";
import Avatar from "@material-ui/core/Avatar";
import TreeItem from "@material-ui/lab/TreeItem";
import {AvatarGroup} from "@material-ui/lab";

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({

    list: {
        width: 250,
    },
    orange: {
        color: theme.palette.getContrastText(deepOrange[500]),
        backgroundColor: deepOrange[500],


    },
    toolbar:{
        background:'#ee002d',
        color:'#ffffff',

    },
    colorWhite:{
        color:'#ffffff',
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
    fullList: {
        width: 'auto',
    },
    root: {
        position: 'fixed',
        bottom: theme.spacing(2),
        right: theme.spacing(2),
        flexGrow: 1,
    },
    treeView: {

    },

    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',

        },
        color:'#ffffff'
    },
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    },
    avatarGroup: {
        left:theme.spacing(90)
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(1),
            width: 'auto',
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    drawerContent:{
        margin: theme.spacing(2),

    },
    inputRoot: {
        color: 'inherit',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },

    drawerPaper: {
        width: drawerWidth,
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
                width: '20ch',
            },
        },
    },
}));

const CommentContainer =(props)=> {
    const handleSelect = (event, nodeIds) => {
        setSelected(nodeIds);
    };
    const handleToggle = (event, nodeIds) => {
        setExpanded(nodeIds);
    };

    const classes = useStyles();
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });
    const [expanded, setExpanded] = React.useState([]);
    const [selected, setSelected] = React.useState([]);
        const [all_comments,setAllcomments]=React.useState([])
    const [total_comments,setTotalComments]=React.useState(0)
    const [slug,setSlug]=React.useState('')
    const [error,setError]=React.useState(null)
    const [commentsRender,setCommentsRender]=React.useState(null)

  React.useEffect(()=>{
      getAllComments(props.slug)
      setAllcomments(props.total_comments)
      setSlug(props.slug)

  },[])

    React.useEffect(()=>{
        if(all_comments.length>0) {
            let comrender = all_comments.map(commentt => {

            return loopComments(commentt)

            });
            setCommentsRender(comrender)
            setTotalComments(all_comments.length)
        }
    },[all_comments])
    const loopComments=(comment)=> {
        if (comment.children.length === 0) {
            return <Comment comment={comment}/>
        } else {
            const commentInstance = Object.assign({}, comment)
            const allChildren = () => {
                return comment.children.map((child, index) =>(loopComments(child))

                )
            }

            return CollapsibleComment(allChildren(),commentInstance)
        }
    }
    const CollapsibleComment = (commentList,commentInstance) => {

console.log(commentList)
        const createAvatar=(avatarLetters)=>{
            return  (<AvatarGroup max={2} className={classes.avatarGroup}>
                <Avatar alt={"default_user"}  className={classes.orange}>{avatarLetters}</Avatar>
                <Avatar alt={"default_user"}  className={classes.orange}>{avatarLetters}</Avatar>
                <Avatar alt={"default_user"}  className={classes.orange}>{avatarLetters}</Avatar>
        </AvatarGroup>)
        }
        return (<TreeItem nodeId={commentInstance.id.toString()} label={commentInstance.creator_name+' 7:00 AM April 14'} key={commentInstance.id.toString()} icon={createAvatar('DE')} className={classes.itemView}> {commentList}</TreeItem>)


    };
const getAllComments=(post_slug)=>{
            axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        axios.defaults.headers = {
          "Content-Type": "application/json",
  };
        axios.get(`${HOST_URL}/api-v1/comments/${post_slug}/`, {

          })
          .then(res => {
             setAllcomments(res.data)


          })
          .catch(err => {
              setError(err)
            console.error(error);

          });



}


    return (

        <div key={props.slug}>

            <CommentTextArea post_slug={props.slug} user_id={1} post_id={props.post_id}/>
            <h5 className="title">{total_comments} Comments</h5>
                                <TreeView

className={classes.treeView}
                                    defaultCollapseIcon={<ExpandMoreIcon />}
                                    defaultExpandIcon={<ChevronRightIcon />}
                                    expanded={expanded}
                                    selected={selected}
                                    onNodeToggle={handleToggle}
                                    onNodeSelect={handleSelect}
                                >



                                    {commentsRender}


                                </TreeView>






            </div>


    );
}


export default CommentContainer;
