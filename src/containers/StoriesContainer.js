import React from "react";
import axios from "axios";
import {HOST_URL} from "../settings";
import Stories1 from "../components/stories/stories1";
class StoriesContainer extends React.Component {
    state = {
        stories:[],

    };
        getStories(){
            axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
            axios.defaults.xsrfCookieName = "csrftoken";
            axios.defaults.headers = {
                "Content-Type": "application/json",
            };
            axios.get(`${HOST_URL}/api-v1/stories/`)
                .then(res => {
                    if (res.status === 200 ) {

                        this.state.stories=res.data;
                        console.log(res.data)

                    }
                })
                .catch(err => {
                    console.error(err);

                });


        }
        componentDidMount() {
            this.getStories()
        }

    render() {

          return (
          <div className="featured-post-area">
        <div className="container">
            <div className="row ">
                <div className="col-md-12 col-centered align-self-md-center">

              <Stories1 allStoriesObjs={this.state.stories}/>


            </div>
             </div>
            </div>
              </div>
          );
        }
      }
      export default StoriesContainer