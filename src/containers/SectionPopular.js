import React from "react";

import image16 from "../assets/img/bg-img/16.jpg"
//import "../assets/style.css"
import PostPoular from "../components/post/post/post_poular";
import PostSmallPoular from "../components/post/post/post_small_poular";
import PostMain from "../components/post/post/post_main";
import PostMain2 from "../components/post/post/post_main2";
import PostSmallMain from "../components/post/post/post_small_main";




const SectionPopular= (props)=>{
    let post_popular_list =null
    let post_popular_small_list =null

    if (props.posts.length >0){
       post_popular_list=props.posts.map(postt => (
            <PostPoular post={postt} image={image16}/>
        ));

    }
    if (props.posts_small.length >0){
        post_popular_small_list=props.posts_small.map(postt => (
            <PostSmallPoular post={postt} image={image16}/>
        ));
    }
    return (
        <div className="popular-news-area section-padding-80-50">
            <div className="container">
                <div className="row">
                    <div className="col-12 col-lg-8">
                        <div className="section-heading">
                            <h6>Popular News</h6>
                        </div>

                        <div className="row">


                       {post_popular_list}
                            </div>
                </div>

                    <div className="col-12 col-lg-4">
                        <div className="section-heading">
                            <h6>Info</h6>
                        </div>

                        <div className="popular-news-widget mb-30">
                            <h3>4 Most Popular News</h3>


                            {post_popular_small_list}
                        </div>


                        <div className="newsletter-widget">
                            <h4>Newsletter</h4>
                            <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                                mus.</p>
                            <form action="#" method="post">
                                <input type="text" name="text" placeholder="Name"/>
                                    <input type="email" name="email" placeholder="Email"/>
                                        <button type="submit" className="btn w-100">Subscribe</button>
                            </form>
                        </div>
                    </div>


            </div>
        </div>
    </div>
    );
  }




export default SectionPopular;
