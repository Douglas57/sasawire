import React from "react";

import SectionMain from "./SectionMain";
import SectionPopular from "./SectionPopular";
import SectionVideo from "./SectionVideo";
import SectionEditors from "./SectionEditors";
import axios from "axios";
import {HOST_URL} from "../settings";
import StoriesContainer from "./StoriesContainer";
import NavNewsSlide from "./NavNewsSlide";
import Box from "@material-ui/core/Box";



class Home extends React.Component {
     state = {
    posts_main:[],
         posts_main_small:[],
         posts_popular:[],
         posts_popular_small:[],
         posts_editors:[],
         posts_editors_small:[],
    error: null,
         breaking:[],
         international:[],
         nav_slide:null

  };
componentDidMount() {
    this.getAllPostsByNBavType()
this.getAllPostsByType(1);
    this.getAllPostsByType(2);
    this.getAllPostsByType(3);
    this.getAllPostsByType(4);
    this.getAllPostsByType(5);
    this.getAllPostsByType(6);

}

getAllPostsByType(post_type){
           axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        axios.defaults.headers = {
          "Content-Type": "application/json",
  };
        axios.get(`${HOST_URL}/api-v1/posts/type/${post_type}/`, {

          })
          .then(res => {
              if (res.status === 200 ) {
                  if (post_type === 1) {
                      this.setState({
                          posts_main: res.data
                      });
                  }
                  if (post_type === 2) {
                      this.setState({
                          posts_main_small: res.data
                      });
                  }
                  if (post_type === 3) {
                      this.setState({
                          posts_popular: res.data
                      });
                  }

                  if (post_type === 4) {
                      this.setState({
                          posts_popular_small: res.data
                      });
                  }
                  if (post_type === 5) {
                      this.setState({
                          posts_editors: res.data
                      });
                  }

                  if (post_type === 6) {
                      this.setState({
                          posts_editors_small: res.data
                      });
                  }

                  console.log(res.data)
              }
          })
          .catch(err => {
            console.error(err);
            this.setState({
              error: err
            });
          });



}
getAllPostsByNBavType(){
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        axios.defaults.headers = {
            "Content-Type": "application/json",
        };
        axios.get(`${HOST_URL}/api-v1/posts/nav/type/`, {

        })
            .then(res => {
                if (res.status === 200 ) {
                    console.log(res.data)
                   this.setState({breaking:res.data.breaking})
                    this.setState({international:res.data.international})


                this.state.nav_slide=(<NavNewsSlide breaking={this.state.breaking} international={this.state.international}/>)}
            })
            .catch(err => {
                console.error(err);
                this.setState({
                    error: err
                });
            });



    }
    render() {
    return (

<div >
    {this.state.nav_slide}
        <StoriesContainer/>
      <SectionMain posts={this.state.posts_main} posts_small={this.state.posts_main_small}/>
      <SectionPopular posts={this.state.posts_popular} posts_small={this.state.posts_popular_small}/>
      <SectionVideo posts={this.state.posts}/>
      <SectionEditors posts={this.state.posts_editors} posts_small={this.state.posts_editors_small}/>

</div>
    );
  }
}



export default Home;
