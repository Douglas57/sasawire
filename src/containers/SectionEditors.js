import React from "react";

import image16 from "../assets/img/bg-img/16.jpg"
//import "../assets/style.css"
import PostEditors from "../components/post/post/post_editors";
import PostSmallEditors from "../components/post/post/post_small_editors";
import PostPoular from "../components/post/post/post_poular";
import PostSmallPoular from "../components/post/post/post_small_poular";



const SectionEditors= (props)=>{
    let post_editors_list =null
    let post_editors_small_list =null

    if (props.posts.length >0){
       post_editors_list=props.posts.map(postt => (
            <PostEditors post={postt} image={image16}/>
        ));

    }
    if (props.posts_small.length >0){
        post_editors_small_list=props.posts_small.map(postt => (
            <PostSmallEditors post={postt} image={image16}/>
        ));
    }
    return (
        <div className="editors-pick-post-area section-padding-80-50">
            <div className="container">
                <div className="row">

                    <div className="col-12 col-md-7 col-lg-9">
                        <div className="section-heading">
                            <h6>Editor’s Pick</h6>
                        </div>

                        <div className="row">


                      { post_editors_list}
                            </div>
                </div>

                    <div className="col-12 col-md-5 col-lg-3">
                        <div className="section-heading">
                            <h6>World News</h6>
                             { post_editors_small_list}
                        </div>
                    </div>


            </div>
        </div>
    </div>
    );
  }




export default SectionEditors;
