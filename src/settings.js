let DEBUG=false;
let HOST_URL = "https://sasawire.com:4000";
let SOCKET_URL = "wss://sasawire.com:4000";
if (DEBUG) {
  HOST_URL = "http://127.0.0.1:8000";
  SOCKET_URL = "ws://127.0.0.1:8000";
}
export { HOST_URL, SOCKET_URL };

