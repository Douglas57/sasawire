import React, {useState} from "react";
import default_image from "../assets/img/default.jpg";
import "../assets/zuckjs/zuck.css"
import "../assets/zuckjs/skins/snapgram.css"
import StorItem from "./stories/storiesItem";
      class Stories2 extends React.Component {
constructor(props) {
    super(props);

}



componentDidMount() {


}

          render() {

              let storyit=this.props.story.story_items.map(it=>(
                  <StorItem item={it} timestamp={this.props.timestam}/>
              ));



          return (

                  <div className="story" data-id={this.props.story.id}
                       data-last-updated={this.props.timestam} data-photo={this.props.story.image}>
                      <a className="item-link" href={default_image}>
          <span className="item-preview">
            <img  src={default_image}/>
          </span>
                          <span className="info" itemProp="author" itemScope="" itemType="http://schema.org/Person">
            <strong className="name" itemProp="name">{this.props.story.owner_name}</strong>
            <span className="time">{this.props.timestam}</span>
          </span>
                      </a>

                      <ul className="items">

                          {storyit}

                      </ul>
                  </div>






          );
        }
      }
      export default Stories2