import React from "react";
import CommentLikeArea from "../../comment/comment_like";
import RenderTimestamp from "../../../utils/timeago";

const PostMain = props => {
    return (
        <div className="single-blog-post featured-post" key={props.post.id}>
            <div className="post-thumb">
                <a href={`/posts/view/${props.post.slug}/`}><img src={props.post.image} alt=""/></a>
            </div>
            <div className="post-data">
                <a href={`/category/${props.post.category_slug}/`} className="post-catagory">{props.post.category_title}</a>
                <a href={`/posts/view/${props.post.slug}/`} className="post-title">
                    <h6>{props.post.title}</h6>
                </a>
                <div className="post-meta">
                    <p className="post-author">Published <a href="#">{RenderTimestamp(props.post.published_on)}</a></p>
                    <p className="post-excerp">{props.post.body} </p>

                    <CommentLikeArea commentsCount={props.post.total_comments} likesCount={0} slug={props.post.slug}/>
                </div>
            </div>
        </div>
    );
};

export default PostMain ;
