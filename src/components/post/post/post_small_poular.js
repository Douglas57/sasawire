import React from "react";
import RenderTimestamp from "../../../utils/timeago";


const PostSmallPoular = props => {
    return (
        <div className="single-popular-post" key={props.post.id}>
            <a href={`/posts/view/${props.post.slug}/`}>
                <h6><span>1.</span> {props.post.title}</h6>
            </a>
            <p>{RenderTimestamp(props.post.created_on)}</p>
        </div>
    );
};

export default PostSmallPoular ;
