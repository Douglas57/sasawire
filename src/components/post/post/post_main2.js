import React from "react";
import CommentLikeArea from "../../comment/comment_like";


const PostMain2 = props => {
    return (
        <div className="single-blog-post featured-post-2" key={props.post.id}>
            <div className="post-thumb">
                <a href={`/posts/view/${props.post.slug}/`}><img src={props.post.image} alt=""/></a>
            </div>
            <div className="post-data">
                <a href={`/category/${props.post.category_slug}/`} className="post-catagory">{props.post.category_title}</a>
                <div className="post-meta">
                    <a href={`/posts/view/${props.post.slug}/`} className="post-title">
                        <h6>{props.post.short_description}</h6>
                    </a>

                    <CommentLikeArea commentsCount={props.post.total_comments} likesCount={0} slug={props.post.slug}/>
                </div>
            </div>
        </div>
    );
};

export default PostMain2 ;
