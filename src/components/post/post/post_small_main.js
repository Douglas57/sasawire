import React from "react";
import RenderTimestamp from "../../../utils/timeago";


const PostSmallMain = props => {
    return (
        <div className="single-blog-post small-featured-post d-flex" key={props.post.id}>
            <div className="post-thumb">
                <a href={`/posts/view/${props.post.slug}/`}><img src={props.post.image} alt=""/></a>
            </div>
            <div className="post-data">
                <a href={`/category/${props.post.category_slug}/`} className="post-catagory">{props.post.category_title}</a>
                <div className="post-meta">
                    <a href={`/posts/view/${props.post.slug}/`} className="post-title">
                        <h6>{props.post.title}</h6>
                    </a>
                    <p className="post-date"> <span>{RenderTimestamp(props.post.created_on)}</span></p>
                </div>
            </div>
        </div>
    );
};

export default PostSmallMain ;
