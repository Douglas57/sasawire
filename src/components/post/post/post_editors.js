import React from "react";
import RenderTimestamp from "../../../utils/timeago";


const PostEditors = props => {

    return (
        <div className="col-12 col-lg-4" key={props.post.id}>
            <div className="single-blog-post" >
                <div className="post-thumb">
                    <a href={`/posts/view/${props.post.slug}/`}><img src={props.post.image} alt=""/></a>
                </div>
                <div className="post-data">
                    <a href={`/posts/view/${props.post.slug}/`} className="post-title">
                        <h6>{props.post.title}</h6>
                    </a>
                    <div className="post-meta">
                        <div className="post-date"><a href="#">{RenderTimestamp(props.post.created_on)}</a></div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PostEditors ;
