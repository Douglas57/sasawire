import React from "react";
import {useHistory} from "react-router";
import "../assets/zuckjs/zuck.css"
import "../assets/zuckjs/skins/snapgram.css"
import {ZuckJS as Zuck} from "zuck.js"
import default_image from "../assets/img/default.jpg";
import * as actions from "../store/actions/auth";
import * as navActions from "../store/actions/nav";
import * as messageActions from "../store/actions/message";
import {createBrowserHistory} from 'history'
import {connect} from "react-redux";
class Stories extends React.Component {
    constructor(props){
        super(props);

        //React ^16.3
        this.storiesElement = React.createRef();

        //this.storiesElement = null;
        this. history = createBrowserHistory({forceRefresh: true});
        this.storiesApi = null;
        console.log(this.addToSTories(this.props.allStories));
        this.state = {
            stories: [
                ...this.addToSTories(this.props.allStories)
            ]
        }
    }

    addToStoryItems(allitems=[]){
        return   allitems.map(itm=>(
            [itm.id,
                "photo",
                3,
                itm.image,
                itm.image,
                '',
                false,
                false,
                this.myTimestamp(itm.created_at)]

        ));
    }
    addToSTories(allStrs=[]){
            console.log(allStrs)
        return allStrs.map(strr=>(
            Zuck.buildTimelineItem(
                strr.id,
                strr.story_items[0].image,
                strr.owner_name,
                "",
                this.myTimestamp(strr.story_items[0].created_at),
                [
                    ...this.addToStoryItems(strr.story_items)
                ]
            )
        ));

    }
    timestamp() {
        var timeIndex = 0;
        var shifts = [35, 60, 60 * 3, 60 * 60 * 2, 60 * 60 * 25, 60 * 60 * 24 * 4, 60 * 60 * 24 * 10];

        var now = new Date();
        var shift = shifts[timeIndex++] || 0;
        var date = new Date(now - shift * 1000);

        return date.getTime() / 1000;

    };
       myTimestamp(dateString) {
           var date1 = new Date(dateString);


        var unixDate = new Date(0);


        var date = new Date( date1-unixDate );

        return date.getTime()/1000;

    };
    handleAddStory=(event)=>{

        event.preventDefault();
        if(this.props.isAuthenticated) {
        this.history.push('/stories/add/imageEditor/')
        }
        else{

            this.history.push('/login')
        }



    }

    componentDidMount() {
        let currentSkin = "snapgram";

        this.storiesApi = new Zuck(this.storiesElement, {
            backNative: true,
            previousTap: true,
            skin: 'snapgram',      // container class
            avatars: true,         // shows user photo instead of last story item preview
            list: false,
            openEffect: true,
            cubeEffect: true,
            autoFullScreen: true, // enables fullscreen on mobile browsers
            backButton: true,      // adds a back button to close the story viewer

            localStorage: true,
            stories: this.state.stories,
            reactive: true,
            callbacks: {
                onDataUpdate: function (currentState, callback) {
                    this.setState(state => {
                        state.stories = currentState;

                        return state;
                    }, () => {
                        callback();
                    });
                }.bind(this)
            }
        });
    }

    render() {
        const timelineItems = []

        this.state.stories.forEach((story, storyId) => {
            const storyItems = [];

            story.items.forEach((storyItem) => {
                storyItems.push(
                    <li key={storyItem.id} data-id={storyItem.id} data-time={storyItem.time} className={(storyItem.seen ? 'seen' : '')}>
                        <a href={storyItem.src} data-type={storyItem.type} data-length={storyItem.length} data-link={storyItem.link} data-linktext={storyItem.linkText}>
                            <img src={storyItem.preview} />
                        </a>
                    </li>
                );
            });

            let arrayFunc = story.seen ? 'push' : 'unshift';
            timelineItems[arrayFunc](
                <div className={(story.seen ? 'story seen' : 'story')} key={storyId} data-id={storyId} data-last-updated={story.lastUpdated} data-photo={story.photo}>
                    <a className="item-link" href={story.link}>
                  <span className="item-preview">
                    <img src={story.photo} />
                  </span>
                        <span className="info" itemProp="author" itemScope="" itemType="http://schema.org/Person">
                    <strong className="name" itemProp="name">{story.name}</strong>
                    <span className="time">{story.lastUpdated}</span>
                  </span>
                    </a>

                    <ul className="items">
                        {storyItems}
                    </ul>
                </div>
            );
        });

        return (
            <div>
                <div ref={node => this.storiesElement = node}  id="stories-react" className="storiesWrapper">
                    <a href="/stories/add/imageEditor/" onClick={this.handleAddStory}><span className="fa fa-plus-circle"style={{"color":"red"}}></span></a>
                    {timelineItems}
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => {
  return {

    token: state.auth.token,
    isAuthenticated: state.auth.token !== null,


  }
}





export default connect(mapStateToProps)(Stories)