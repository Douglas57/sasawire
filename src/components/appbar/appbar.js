import React from 'react';
import clsx from 'clsx';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { fade,makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import InputBase from '@material-ui/core/InputBase';
import Fab from '@material-ui/core/Fab';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Zoom from '@material-ui/core/Zoom';
import Drawer from '@material-ui/core/Drawer';
import SearchIcon from '@material-ui/icons/Search';

import Divider from '@material-ui/core/Divider';
import Link from '@material-ui/core/Link';
import { Link as RouterLink } from 'react-router-dom'
import {Mail} from "@material-ui/icons";
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import axios from "axios";
import {HOST_URL} from "../../settings";
import LogoImage from "../../assets/img/core-img/log.jpeg";
import defaultProfile from "../../assets/img/default.jpg";
import NavNewsSlide from "../../containers/NavNewsSlide";
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';
import Avatar from '@material-ui/core/Avatar';
import {createBrowserHistory} from 'history'
import {NavLink} from "react-router-dom";
import * as actions from "../../store/actions/auth";
import {connect} from "react-redux";

const history = createBrowserHistory({forceRefresh: true});
const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({


    list: {
        width: 250,
    },

    toolbar:{
        background:'#ee002d',
        color:'#ffffff',

    },
    colorWhite:{
        color:'#ffffff',
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
    fullList: {
        width: 'auto',
    },
    root: {

        position:'fixed',
        zIndex:1,
        bottom: theme.spacing(2),
        right: theme.spacing(2),


    },
    treeView: {
        height: 216,
        flexGrow: 1,
        maxWidth: 400,
    },
    boxContainer: {
        display:'block',
        marginTop:theme.spacing(2)

    },

    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',

        },
        color:'#ffffff'
    },
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(1),
            width: 'auto',
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    drawerContent:{
        margin: theme.spacing(2),

    },
    inputRoot: {
        color: 'inherit',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },

    drawerPaper: {
        width: drawerWidth,
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
                width: '20ch',
            },
        },
    },
}));

function ScrollTop(props) {
    const { children, window } = props;
    const classes = useStyles();
    // Note that you normally won't need to set the window ref as useScrollTrigger
    // will default to window.
    // This is only being set here because the demo is in an iframe.
    const trigger = useScrollTrigger({
        target: window ? window() : undefined,
        disableHysteresis: true,
        threshold: 100,
    });

    const handleClick = (event) => {
        const anchor = (event.target.ownerDocument || document).querySelector('#back-to-top-anchor');

        if (anchor) {
            anchor.scrollIntoView({ behavior: 'smooth', block: 'center' });
        }
    };

    return (
        <Zoom in={trigger}>
            <div onClick={handleClick} role="presentation" className={classes.root}>
                {children}
            </div>
        </Zoom>
    );
}

ScrollTop.propTypes = {
    children: PropTypes.element.isRequired,
    /**
     * Injected by the documentation to work in an iframe.
     * You won't need it on your project.
     */
    window: PropTypes.func,
};
const ResponsiveDrawer=(props)=> {
    const classes = useStyles();
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });
    const [expanded, setExpanded] = React.useState([]);
    const [selected, setSelected] = React.useState([]);
    const [nothing, setNothing] = React.useState(null);
    const [open, setOpen] = React.useState(false);
    const [categories,setCategories]=React.useState([])
    const [categoryList, setCategoryList] = React.useState(null);
    const handleToggle = (event, nodeIds) => {
        setExpanded(nodeIds);
    };

React.useEffect(()=> {
     getAllCategories();

},[])
    const handleSelect = (event, nodeIds) => {
        setSelected(nodeIds);
    };

    const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({ ...state, [anchor]: open });
    };


    const singleCategoryObject=(category)=>{


        return    (<TreeItem nodeId={category.id.toString()} label={category.title} key={category.id.toString()} className={classes.drawerContent} onLabelClick={(event)=>{
            event.preventDefault()
            history.push('/category/'+category.slug)
        }}/>)
    }

    const  multipleCategoryObject=(category,list)=>{



        return (<TreeItem nodeId={category.id.toString()} label={category.title} key={category.id.toString()} className={classes.drawerContent}>{list}</TreeItem>)

    }

    const  loopCategories=(singLeCategory)=>{

        if(singLeCategory.children.length === 0){
            return singleCategoryObject(singLeCategory)
        }
        else {
            // loop children

            const cat=Object.assign({},singLeCategory)
            const allchildren=()=>{
                return singLeCategory.children.map((child,index)=>
                    loopCategories(child)
                )

            }


            return multipleCategoryObject(cat,allchildren())

        }



    }
    const getAllCategories=()=>{
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        axios.defaults.headers = {
            "Content-Type": "application/json",
        };
        axios.get(`${HOST_URL}/api-v1/categories/`, {

        })
            .then(res => {
                console.log(categories)
                console.log([...res.data])
                let looped=res.data.map((categoryObj) => {
                    return loopCategories(categoryObj)


                })
                console.log(looped)
                setNothing(looped)
                console.log(nothing)


            })
            .catch(err => {
                console.error(err);



            });



    }
    let loopCat=null
    if(categories!==null && categories.length>0) {
        loopCat = categories.map((categoryObj) => {
            return loopCategories(categoryObj)


        });
       setCategoryList((<div
           className={clsx(classes.list, {
               [classes.fullList]: 'left' === 'top' || 'left' === 'bottom',
           })}
           role="presentation"
           onClick={toggleDrawer('left', false)}
           onKeyDown={toggleDrawer('left', false)}
       >
           {loopCat}
       </div>))
        console.log(categoryList)
    }
    return (


        <React.Fragment>
            <CssBaseline />
            <AppBar >
                <Toolbar  className={classes.toolbar}>


                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                edge="start"
                                className={classes.menuButton}
                                onClick={toggleDrawer('left', true)}

                            >
                                <MenuIcon />
                            </IconButton>
                            {/*<Button onClick=>{anchor}</Button>*/}
                            <Drawer
                                className={classes.drawer}

                                classes={{
                                    paper: classes.drawerPaper,
                                }}
                                anchor={'left'}
                                    open={state['left']}
                                    onClose={toggleDrawer('left', false)}>

                                <TreeView
                                    className={classes.treeView}
                                    defaultCollapseIcon={<ExpandMoreIcon />}
                                    defaultExpandIcon={<ChevronRightIcon />}
                                    expanded={expanded}
                                    selected={selected}
                                    onNodeToggle={handleToggle}
                                    onNodeSelect={handleSelect}
                                >
                                     <a href={'/'}>
                                    <Typography variant="h4" noWrap className={classes.drawerContent} >
                                       Home
                                    </Typography>
                                     </a>
                                    <Typography variant="h6" noWrap className={classes.drawerContent}>
                                        Categories
                                    </Typography>
                                    <Divider/>
                                    {nothing}
                                    <Divider/>
                                    <Typography variant="h6" noWrap className={classes.drawerContent}>
                                        Account
                                    </Typography>
                                    <Divider/>
                                    {props.isAuthenticated
                                        ?
                                        (
                                            <div className="">
                                                <Button color="secondary" className={classes.drawerContent}
                                                        onClick={props.logout}>Logout {props.usernamee} </Button>


                                            </div>
                                        )

                                        :
                                        (
                                            <div>
                                            <Button href="/login" color="secondary" className={classes.drawerContent}>
                                                Login
                                            </Button>

                                        <Button href="/register" color="primary" className={classes.drawerContent}>
                                        Singup
                                        </Button>
                                            </div>
                                        )
                                    }


                                </TreeView>
                            </Drawer>


                    <a href="/" className={classes.title}>
                        <Typography variant="h4" className={classes.title}> Sasawire</Typography>
                    </a>

                    <div className={classes.search}>

                        <div className={classes.searchIcon}>
                            <SearchIcon />
                        </div>
                        <InputBase
                            placeholder="Search…"
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    </div>
                    {props.isAuthenticated
                        ?
                        (
                            <div className="">
                                <Avatar alt={"default_user"} src={defaultProfile} className={classes.large}/>
                                <Button className={classes.colorWhite}
                                        onClick={props.logout}>Logout {props.usernamee}</Button>

                            </div>
                        )

                        :
                        (
                            <div>
                                <Button href={'/login'} className={classes.colorWhite}>Login</Button>
                                <Button href={'/register'} className={classes.colorWhite}>Signup</Button>
                            </div>
                        )
                    }


                </Toolbar>
            </AppBar>

            <Container>

<Box className={classes.boxContainer}>
    <Toolbar id="back-to-top-anchor" />

                    {props.children}



</Box>

            </Container>
            <footer className="footer-area">
                {/*<div className="main-footer-area">*/}
                {/*    <div className="container">*/}
                {/*        <div className="row">*/}


                {/*            <div className="col-12 col-sm-6 col-lg-4">*/}
                {/*                <div className="footer-widget-area mt-80">*/}

                {/*                    <div className="footer-logo">*/}
                {/*                        <a href={``}><img src={LogoImage} alt=""/></a>*/}
                {/*                    </div>*/}

                {/*                    <ul className="list">*/}
                {/*                        <li><a href="mailto:contact@youremail.com">contact@youremail.com</a></li>*/}
                {/*                        <li><a href="tel:+4352782883884">+43 5278 2883 884</a></li>*/}
                {/*                        <li><a href="http://yoursitename.com">www.yoursitename.com</a></li>*/}
                {/*                    </ul>*/}
                {/*                </div>*/}
                {/*            </div>*/}


                {/*            <div className="col-12 col-sm-6 col-lg-2">*/}
                {/*                <div className="footer-widget-area mt-80">*/}

                {/*                    <h4 className="widget-title">Politics</h4>*/}

                {/*                    <ul className="list">*/}
                {/*                        <li><a href="#">Business</a></li>*/}
                {/*                        <li><a href="#">Markets</a></li>*/}
                {/*                        <li><a href="#">Tech</a></li>*/}
                {/*                        <li><a href="#">Luxury</a></li>*/}
                {/*                    </ul>*/}
                {/*                </div>*/}
                {/*            </div>*/}


                {/*            <div className="col-12 col-sm-4 col-lg-2">*/}
                {/*                <div className="footer-widget-area mt-80">*/}

                {/*                    <h4 className="widget-title">Featured</h4>*/}

                {/*                    <ul className="list">*/}
                {/*                        <li><a href="#">Football</a></li>*/}
                {/*                        <li><a href="#">Golf</a></li>*/}
                {/*                        <li><a href="#">Tennis</a></li>*/}
                {/*                        <li><a href="#">Motorsport</a></li>*/}
                {/*                        <li><a href="#">Horseracing</a></li>*/}
                {/*                        <li><a href="#">Equestrian</a></li>*/}
                {/*                        <li><a href="#">Sailing</a></li>*/}
                {/*                        <li><a href="#">Skiing</a></li>*/}
                {/*                    </ul>*/}
                {/*                </div>*/}
                {/*            </div>*/}


                {/*            <div className="col-12 col-sm-4 col-lg-2">*/}
                {/*                <div className="footer-widget-area mt-80">*/}

                {/*                    <h4 className="widget-title">FAQ</h4>*/}

                {/*                    <ul className="list">*/}
                {/*                        <li><a href="#">Aviation</a></li>*/}
                {/*                        <li><a href="#">Business</a></li>*/}
                {/*                        <li><a href="#">Traveller</a></li>*/}
                {/*                        <li><a href="#">Destinations</a></li>*/}
                {/*                        <li><a href="#">Features</a></li>*/}
                {/*                        <li><a href="#">Food/Drink</a></li>*/}
                {/*                        <li><a href="#">Hotels</a></li>*/}
                {/*                        <li><a href="#">Partner Hotels</a></li>*/}
                {/*                    </ul>*/}
                {/*                </div>*/}
                {/*            </div>*/}


                {/*            <div className="col-12 col-sm-4 col-lg-2">*/}
                {/*                <div className="footer-widget-area mt-80">*/}

                {/*                    <h4 className="widget-title">+More</h4>*/}

                {/*                    <ul className="list">*/}
                {/*                        <li><a href="#">Fashion</a></li>*/}
                {/*                        <li><a href="#">Design</a></li>*/}
                {/*                        <li><a href="#">Architecture</a></li>*/}
                {/*                        <li><a href="#">Arts</a></li>*/}
                {/*                        <li><a href="#">Autos</a></li>*/}
                {/*                        <li><a href="#">Luxury</a></li>*/}
                {/*                    </ul>*/}
                {/*                </div>*/}
                {/*            </div>*/}
                {/*        </div>*/}
                {/*    </div>*/}
                {/*</div>*/}


                <div className="bottom-footer-area">
                    <div className="container ">
                        <div className="row  align-items-center">
                            <div className="col-12">

                                <p>
                                    Copyright &copy;
                                    <script>document.write(new Date().getFullYear());</script>
                                    All rights reserved |  made with <i className="fa fa-heart-o"
                                                                                        aria-hidden="true"></i> by <a
                                    href="" target="_blank">Douglas</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>


            <ScrollTop {...props} >

                <Fab color="secondary" size="large" aria-label="scroll back to top">
                    <KeyboardArrowUpIcon  fontSize={'large'}/>
                </Fab>

            </ScrollTop>

        </React.Fragment>

    );

}

const mapStateToProps = state => {
    return {

        authenticated: state.auth.token,
        isAuthenticated: state.auth.token !== null,
        usernamee: state.auth.username

    }
}

const mapDispatchToProps = dispatch => {
    return {
        onTryAutoSignup: () => dispatch(actions.authCheckState()),
        logout:()=>dispatch(actions.logout())
    };
};

export default connect(mapStateToProps, mapDispatchToProps) (ResponsiveDrawer);
