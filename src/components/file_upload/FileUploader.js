import React, { useState } from 'react';

import axios from 'axios';

const API_BASE = "http://localhost:5000"

function submitForm(contentType, data, setResponse) {
    axios({
        url: `${API_BASE}/upload`,
        method: 'POST',
        data: data,
        headers: {
            'Content-Type': contentType
        }
    }).then((response) => {
        setResponse(response.data);
    }).catch((error) => {
        setResponse("error");
    })
}

export default function FileUploader() {
    const [title, setTitle] = useState("");
    const [file, setFile] = useState(null);
    const [filename, setFilename] = useState(null);
    const [desc, setDesc] = useState("");
    const handleFileChange=(event)=>{
        console.log(URL.createObjectURL(event.target.files[0]))
        let image = document.getElementById('cro-image');
        console.log(image)
        image.src = URL.createObjectURL(event.target.files[0]);
    };
    const readURL=(ev)=>{
        if (ev.target.files[0]) {
            var reader = new FileReader();
            const imgPreview = document.getElementById("cro-image");
            reader.onload = function (e) {
             imgPreview.setAttribute('src',e.target.result);
             imgPreview.remove();

            reader.readAsDataURL(ev.target.files[0]);
        }
    }}
    function uploadWithFormData(){

    }

    function uploadWithJSON(){

    }

    return (
        <div className="App">
            <h2>Upload Form</h2>
            <form>
                <label>
                    File Title
                    <input type="text" vaue={title}
                           onChange={(e) => { setTitle(e.target.value )}}
                           placeholder="Give a title to your upload" />
                </label>

                <label>
                    File
                    <input type="file" name="file"  onchange={readURL} id={'file-choose'}/>
                </label>

                <label>
                    Description
                    <textarea value={desc} onChange={(e) => setDesc(e.target.value)}></textarea>
                </label>

                <input type="button" value="Upload as Form" onClick={uploadWithFormData} />
                <input type="button" value="Upload as JSON" onClick={uploadWithJSON}/>
            </form>
        </div>
    );
}
