import React ,{useState}from 'react'

import * as authActions from "../../store/actions/auth";
import {connect} from "react-redux";


 const LoginForm=props=>{
     const [username,setUserName]=useState(null);
     const [password,setPassword]=useState(null);

     const submitLogin=(e)=>{
         e.preventDefault();
         props.login(username,password)

     };

    return (
        <div className='row'>
            <div className='col-md-6 offset-md-3'>
                <div className="contact-form-area">
                    <form action="#" method="post">
                        <fieldset className="form-group">
                            <legend className="border-bottom mb-4 text-center h2" >Sign In</legend>


                        <div className="row">
                            <div className="col-12">
                                <input type="text" className="form-control" id="username" name='username' placeholder="username"
                                onChange={(event => setUserName(event.target.value))}
                                />
                            </div>

                            <div className="col-12">
                                <input type="password" className="form-control" id="password" name='password1' placeholder="password"
                                       onChange={(event => setPassword(event.target.value))}
                                />
                            </div>



                            <div className="col-12 text-center">
                                <button className="btn newspaper-btn mt-30 w-100" type="submit" disabled={props.loading} onClick={submitLogin}>{props.loading?"Sending...":"Sign In"}</button>
                            </div>
                            {props.loading?
                            null
                                :
                                <div className="border-top pt-3">
                                    <small className="text-muted">
                                        New here? <a className="ml-2" href={'/register/'}>Signup</a>
                                    </small>
                                </div>
                            }
                        </div>
                        </fieldset>
                    </form>
                </div>

            </div>
        </div>
    )
};
const mapStateToProps = state => {
    return {

        loading: state.auth.loading,

    };
};

const mapDispatchToProps = dispatch => {
    return {
        login: (userName, password) =>
            dispatch(authActions.authLogin(userName, password)),

    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginForm);


