import React ,{useState}from 'react'
import * as authActions from "../../store/actions/auth";
import {connect} from "react-redux";


const RegisterForm=props=>{
    const [username,setUserName]=useState(null);
    const [first_name,setFirst_name]=useState(null);
    const [last_name,setLast_name]=useState(null);
    const [password1,setPassword1]=useState(null);
    const [password2,setPassword2]=useState(null);
    const [email,setEmail]=useState(null);
    const submitRegister=(e)=>{
        e.preventDefault();
        props.signup(first_name,last_name,username,email,password1,password2)
    };

    return (
        <div className='row'>
            <div className='col-md-6 offset-md-3'>
                <div className="contact-form-area">
                    <form action="#" method="post">
                        <fieldset className="form-group">
                            <legend className="border-bottom mb-4 text-center h2" >Sign In</legend>


                            <div className="row">
                                <div className="col-6">
                                    <input type="text" className="form-control" id="first_name" name='first_name' placeholder="first name"
                                           required={true}
                                           onChange={(event => setFirst_name(event.target.value))}
                                    />
                                </div>
                                <div className="col-6">
                                    <input type="text" className="form-control" id="last_name" name='last_name' placeholder="last name"
                                           required={true}
                                           onChange={(event => setLast_name(event.target.value))}
                                    />
                                </div>
                                <div className="col-12">
                                    <input type="text" className="form-control" id="username" name='username' placeholder="username"
                                           required={true}
                                           onChange={(event => setUserName(event.target.value))}
                                    />
                                </div>
                                <div className="col-12">
                                    <input type="text" className="form-control" id="email" name='email' placeholder="email"
                                         required={true}  onChange={(event => setEmail(event.target.value))}
                                    />
                                </div>



                                <div className="col-6">
                                    <input type="password" className="form-control" id="password1" name='password1' placeholder="password"
                                           required={true}
                                           onChange={(event => setPassword1(event.target.value))}
                                    />
                                </div>
                                <div className="col-6">
                                    <input type="password" className="form-control" id="password2" name='password2' placeholder="password"
                                           required={true}
                                           onChange={(event => setPassword2(event.target.value))}
                                    />
                                </div>



                                <div className="col-12 text-center">
                                    <button className="btn newspaper-btn mt-30 w-100" type="submit" disabled={props.loading} onClick={submitRegister}>{props.loading?"Sending...":"Sign Up"}</button>
                                </div>
                                {props.loading?
                                    null
                                    :
                                    <div className="border-top pt-3">
                                        <small className="text-muted">
                                            New here? <a className="ml-2" href={'/login/'}>sign in</a>
                                        </small>
                                    </div>
                                }
                            </div>
                        </fieldset>
                    </form>
                </div>

            </div>
        </div>
    )
};
const mapStateToProps = state => {
    return {

        loading: state.auth.loading,

    };
};

const mapDispatchToProps = dispatch => {
    return {
        signup: (first_name,last_name,username, email, password1, password2) =>
            dispatch(authActions.authSignup(first_name,last_name,username, email, password1, password2)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterForm);

