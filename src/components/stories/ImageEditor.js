
import default_image from "../../assets/img/default.jpg";
import React, {useState,useRef,useEffect} from 'react';
import Cropper from 'cropperjs';
import 'cropperjs/dist/cropper.css';


import axios from "axios";
import {HOST_URL} from "../../settings";
import * as authActions from "../../store/actions/auth";
import {connect} from "react-redux";
import {createBrowserHistory} from 'history'

const history = createBrowserHistory({forceRefresh: true});

const MyImageEditor =props=>{
    const createStory=(formm)=>{
        if(props.isAuthenticated){
            console.log(formm);
            axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
            axios.defaults.xsrfCookieName = "csrftoken";
            axios.defaults.headers = {
                "Content-Type": "application/json",
                Authorization: "Token "+props.token
            };

            axios.post(`${HOST_URL}/api-v1/stories/create/`, formm)
                .then(res => {
                    this.setState(
                        //   {all_comments:res.data}
                    );

                    history.push('/')

                })
                .catch(err => {
                    history.push('/')

                });
        }
        else {
            history.push('/login')
        }



    }
const [imagUrl,setImageUrl]=useState(null);
    const [formd,setFormD]=useState(null);
    const [formImage,setFormImage]=useState(null);
const imageRef=useRef(null);


const sty={
    "display": "block;",

    /* This rule is very important, please don't ignore this */
    "max-width": "100%;"
};
    const submitStory=(event)=>{
    createStory(formd,formImage)
    };

        const readURL=(event)=>{
            setImageUrl(URL.createObjectURL(event.target.files[0]));
            imageRef.current.src=URL.createObjectURL(event.target.files[0]);
            const image = document.getElementById('crop-image');
            const cropper1=new Cropper(image, {
                aspectRatio: 16 / 9,
                crop(event) {
                    console.log(event.detail.x);
                    console.log(event.detail.y);
                    console.log(event.detail.width);
                    console.log(event.detail.height);
                    console.log(event.detail.rotate);
                    console.log(event.detail.scaleX);
                    console.log(event.detail.scaleY);
                    cropper1.getCroppedCanvas().toBlob((blob) => {
                        const formData = new FormData();

                        // Pass the image file name as the third parameter if necessary.
                        formData.append('image', blob  ,'test.png');

                        setFormD(formData);
                        setFormImage(formData.get('image'));
                        console.log(formData.get('image'))
                        // Use `jQuery.ajax` method for example
                        // $.ajax('/path/to/upload', {
                        //     method: 'POST',
                        //     data: formData,
                        //     processData: false,
                        //     contentType: false,
                        //     success() {
                        //         console.log('Upload success');
                        //     },
                        //     error() {
                        //         console.log('Upload error');
                        //     },
                        // });
                    }, 'image/png' );
                },
            });


            cropper1.getCroppedCanvas({
                width: 160,
                height: 90,
                minWidth: 256,
                minHeight: 256,
                maxWidth: 4096,
                maxHeight: 4096,
                fillColor: '#fff',
                imageSmoothingEnabled: false,
                imageSmoothingQuality: 'high',
            });

// Upload cropped image to server if the browser supports `HTMLCanvasElement.toBlob`.
// The default value for the second parameter of `toBlob` is 'image/png', change it if necessary

           };

        return (
            <div>
                <br/>
                <br/>
                <br/>
            <div className='row'>

            <div className='col-md-6 offset-md-3'>
                <div className="contact-form-area">
                <form>
<legend className="border-bottom mb-4 text-center h2" >New Story</legend>
<div className='form-group'>
                    <label>
                        Add Photo</label>
                        <input type="file" name="file" className='form-control'  onChange={readURL} id={'file-choose'} />
</div>


                </form>

            <img id='crop-image' src={imagUrl}
    ref={imageRef} style={sty}

    />
<button className={`btn btn-block btn-success ${imagUrl===null ?'disabled':''}`} onClick={submitStory} >Send</button>
            </div>
            </div>
            </div>
            </div>
        );
    };

const mapStateToProps = state => {
    return {

        token: state.auth.token,
    isAuthenticated: state.auth.token !== null,


    };
};



export default connect(
    mapStateToProps

)(MyImageEditor);