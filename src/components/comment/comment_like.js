import React,{useState} from "react";
import ChatImage from "../../assets/img/core-img/chat.png";
import LikeImage from "../../assets/img/core-img/like.png";

import 'emoji-mart/css/emoji-mart.css';

import {Picker} from "emoji-mart";
import '../../assets/css/emoji-mart-like-coment.css'
const CommentLikeArea = props => {
const [reactionShown,setReactionShown]=useState(false)
    const [selectedEmojis,setSelectedEmojis]=useState(null)
    const onReactionClick=(event)=>{
    event.preventDefault()
            setReactionShown(!reactionShown)
    }
   const handleEmojiSelect = (emoji) => {
        setSelectedEmojis([...selectedEmojis, emoji])
    }
    const customReactionEmojis = [
        {
            name: '+1',
            short_names: ['+1'],
            text: '',
            emoticons: [],
            keywords: ['thumbsup'],
        },

        {
            name: 'heart',
            short_names: ['heart'],
            text: '',
            emoticons: [],
            keywords: ['heart'],
        },
        {
            name: 'grinning',
            short_names: ['grinning'],
            text: '',
            emoticons: [],
            keywords: ['grinning'],
        },
        {
            name: 'open_mouth',
            short_names: ['open_mouth'],
            text: '',
            emoticons: [],
            keywords: ['open_mouth'],
        },
        {
            name: 'sleepy',
            short_names: ['sleepy'],
            text: '',
            emoticons: [],
            keywords: ['sleepy'],
        },
        {
            name: 'hot_face',
            short_names: ['hot_face'],
            text: '',
            emoticons: [],
            keywords: ['hot_face'],
        },

    ]
    return (

        <div className="post-meta d-flex align-items-center">
            <a href="#" className="post-like" onClick={onReactionClick}><img src={LikeImage} alt=""/> <span> {props.likesCount} </span></a>
            <a href={`/posts/view/${props.slug}/`} className="post-comment"><img src={ChatImage} alt=""/> <span>{props.commentsCount}</span></a>
            {reactionShown &&
            <div className="reactions">
                <Picker
                    style={{ position: "absolute" }}
                    showPreview={false}
                    showSkinTones={false}
                    perLine={6}
                    include={['custom']}
                    custom={customReactionEmojis}
                    onSelect={handleEmojiSelect}
                />
            </div>
            }
        </div>
    );
};

export default CommentLikeArea ;
