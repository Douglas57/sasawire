import React from "react";
import default_image from "../../assets/img/default.jpg";
import TreeItem from "@material-ui/lab/TreeItem";
import {makeStyles} from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import { deepOrange, deepPurple } from '@material-ui/core/colors';
import RenderTimestamp from "../../utils/timeago";


const useStyles = makeStyles((theme) => ({
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    itemView: {


    },
    orange: {
        color: theme.palette.getContrastText(deepOrange[500]),
        backgroundColor: deepOrange[500],
        right:theme.spacing(3),

    },
    purple: {
        color: theme.palette.getContrastText(deepPurple[500]),
        backgroundColor: deepPurple[500],
    },
}))

const Comment = props => {
    const classes = useStyles();

const createAvatar=(avatarLetters)=>{
    return  (<Avatar alt={"default_user"}  className={classes.orange}>{avatarLetters}</Avatar>)
}
    return (
        <div key={props.comment.id+'div-key'}>
        <TreeItem nodeId={props.comment.id.toString()} label={props.comment.creator_name+' '+RenderTimestamp(props.comment.created_at)} key={props.comment.id.toString()} icon={createAvatar(props.comment.creator_name.toString().charAt(0))} className={classes.itemView}/>
            <p key={props.comment.id+'just-key'}>{props.comment.body}</p>
        </div>

    );
};

export default Comment ;
