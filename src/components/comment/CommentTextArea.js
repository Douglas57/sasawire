import React from "react";
import 'emoji-mart/css/emoji-mart.css'
import { Picker } from 'emoji-mart'
import axios from "axios";
import {HOST_URL} from "../../settings";
import {connect} from "react-redux";
import {createBrowserHistory} from 'history'

const history = createBrowserHistory({forceRefresh: true});

class CommentTextArea extends React.Component {
    constructor(props) {
        super(props);
        // Don't call this.setState() here!

    }
    state={
        newComment:'',
        post_id:this.props.post_id,
        post_slug:this.props.post_slug,
        user_id:this.props.user_id,
        showEmojis: false,
        parent_comment:null,
        errors: []
    };



    showEmojisFunc =(e) => {
        this.setState(
            {
                showEmojis: true
            },
            (e) => document.addEventListener("click", this.closeMenu)
        );

    };


    closeMenu = e => {
        console.log(this.emojiPicker);
        if (this.emojiPicker !== null && this.emojiPicker.contains(e.target)) {
            this.setState(
                {
                    showEmojis: false
                },() => document.removeEventListener("click", this.closeMenu)
            );
        }
    };
    handleChange = e => {

        //console.log(validation)
        this.setState({
            newComment: e.target.value,
            errors:[]
        });
    };

    handleSubmit = e => {
        e.preventDefault();

        console.log(this.state.newComment)
        if (this.state.newComment.length > 0) {
            this.createComment(this.state.post_slug);
            this.setState({ newComment: "" });
        }
        else{
            this.setState({errors:['kindly write a comment before submitting!!!']})}

    };

    addEmoji = e => {
        // console.log(e.native);
        let emoji = e.native;
        this.setState({
            newComment: this.state.newComment + emoji
        });
    };
    createComment(post_slug){
        if(this.props.isAuthenticated) {
            axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
            axios.defaults.xsrfCookieName = "csrftoken";
            axios.defaults.headers = {
                "Content-Type": "application/json",
                Authorization: "Token " + this.props.token
            };
            axios.post(`${HOST_URL}/api-v1/comments/create/${post_slug}/`, {

                body: this.state.newComment,
                post: this.state.post_id,
                parent_comment: this.state.parent_comment
            })
                .then(res => {
                    this.setState(
                        history.forceRefresh
                        //   {all_comments:res.data}
                    );

                    console.log(res.data)

                })
                .catch(err => {
                    console.error(err);
                    this.setState({
                        error: err
                    });
                });
        }
        else {
            history.push('/login')
        }



    }
    render(){
        let errors = this.state.errors.map(err => <p style={{ color: "red" }}>{err}</p>);
        return (
            <div className="post-a-comment-area section-padding-80-0">
                <h4>Leave a comment  	 	&#x1F600;  </h4>


                <div className="contact-form-area">
            <div style={styles.container} className="newMessageForm">
                <form style={styles.form} onSubmit={this.handleSubmit}>
                    {this.state.showEmojis ? (
                        <span style={styles.emojiPicker} ref={el => (this.emojiPicker = el)}>
            <Picker
                onSelect={this.addEmoji}
                emojiTooltip={false}
                title=""
            />
          </span>
                    ) : (
                        <p style={styles.getEmojiButton} onClick={this.showEmojisFunc}>
                            {String.fromCodePoint(0x1f60a)}
                        </p>
                    )}
                    <input
                        style={styles.input}
                        type="text"

                        value={this.state.newComment}
                        onChange={this.handleChange}
                        placeholder="Comment..."
                    />
                    <button  className='btn btn-default' type={'submit'} ><i className='fa fa-chevron-circle-right'></i> </button>
                </form>
                <div >{errors}</div>

            </div>
                </div>
            </div>

                //<Picker set='google'  title={tt} emoji='point_up' onSelect={(emoji) => {console.log(emoji) ;tt=emoji.unifiedToNative}}/>
                // <Picker onSelect={this.addEmoji} />
                // <Picker title='Pick your emoji…' emoji='point_up' />
                // <Picker style={{ position: 'absolute', bottom: '20px', right: '20px' }} />
                // <Picker i18n={{ search: 'Recherche', categories: { search: 'Résultats de recherche', recent: 'Récents' } }} />

        )
    }
};

const mapStateToProps = state => {
    return {

        token: state.auth.token,
        isAuthenticated: state.auth.token !== null,


    };
};



export default connect(
    mapStateToProps

)(CommentTextArea);
const styles = {
    container: {
        padding: 20,
        borderTop: "1px #4C758F solid",
        marginBottom: 20
    },
    form: {
        display: "flex"
    },
    input: {
        color: "inherit",
        background: "none",
        outline: "none",
        border: "none",
        flex: 1,
        fontSize: 16
    },
    getEmojiButton: {

        cssFloat: "left",
        left: 0,
        border: "none",
        marginTop: "10px",
        cursor: "pointer"
    },
    emojiPicker: {
        position: "absolute",
        bottom: 10,
        left: 0,
        cssFloat: "left",
        marginRight: "200px"
    }
};

