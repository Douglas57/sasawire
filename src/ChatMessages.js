import React from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import BaseRouter from "./routes";
import Sidepanel from "./containers/Sidepanel";
import Profile from "./containers/Profile";
import AddChatModal from "./containers/Popup";
import * as actions from "./store/actions/auth";
import * as navActions from "./store/actions/nav";
import * as messageActions from "./store/actions/message";
import WebSocketInstance from "./websocket";
import "./assets/chat/css/style.css";
import ChatRouter from "./chatsroute";
import Hoc from "./hoc/hoc";
import Chat from "./containers/Chat";


class ChatMessages extends React.Component {
  componentDidMount() {
    this.props.onTryAutoSignup();
    const chatcom={...this.state,chatModule: (<Chat chatId={this.props.match.params.chatID}/>)}
    this.setState(chatcom)

    console.log()

  }
state={
    chatModule:null
};
  constructor(props) {
    super(props);
    WebSocketInstance.addCallbacks(
      this.props.setMessages.bind(this),
      this.props.addMessage.bind(this)
    );
  }

  render() {
    return (


          <div id="frame">
            <Sidepanel />
            <div className="content">
              <AddChatModal
                  isVisible={this.props.showAddChatPopup}
                  close={() => this.props.closeAddChatPopup()}
              />

              <Profile />

              {this.state.chatModule}

            </div>


            </div>



    );
  }
}

const mapStateToProps = state => {
  return {
    showAddChatPopup: state.nav.showAddChatPopup,
    authenticated: state.auth.token,
    token:state.auth.token
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState()),
    closeAddChatPopup: () => dispatch(navActions.closeAddChatPopup()),
    addMessage: message => dispatch(messageActions.addMessage(message)),
    setMessages: messages => dispatch(messageActions.setMessages(messages))
  };
};

export default connect(
  mapStateToProps,mapDispatchToProps

)(ChatMessages);
