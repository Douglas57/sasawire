# Generated by Django 3.1.1 on 2021-01-20 00:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('category', '0002_auto_20201017_0230'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='image',
            field=models.ImageField(blank=True, max_length=255, null=True, upload_to='categories'),
        ),
    ]
