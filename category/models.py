from django.db import models
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from PIL import Image
from django.utils import timezone

from .utils import unique_slug_generator


User = get_user_model()
def posts_directory_path(instance, filename):

    return 'categories/{0}/'.format(instance.id)

class Category(models.Model):
    """Model For Blog Posts"""

    title = models.CharField(max_length=255)
    description = models.TextField()
    creator = models.ForeignKey(
        User, on_delete=models.SET(None), related_name='categories', related_query_name='categories',null=True)
    parent_category = models.ForeignKey(
       "self",on_delete=models.SET(None), related_name='children', related_query_name='children', null=True,blank=True)
    slug = models.SlugField(blank=True, null=True)
    image = models.ImageField( upload_to='categories',null=True,max_length=255,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    @property
    def creator_name(self):
        try:
            if self.creator.first_name:
                return f'{self.creator.first_name} {self.creator.last_name}'
            else:
                return f'{self.creator.username}'
        except:
            return 'Name not set'

    @property
    def children(self):
        return self.children.objects.all()

    class Meta:
        indexes = [models.Index(fields=['slug'])]
        ordering = ['-created_at']


@receiver(post_save, sender=Category)
def generate_unique_slug_for_categories(sender, instance, created, *args, **kwargs):

    if created:
        instance.slug = unique_slug_generator(instance)
        instance.save()


