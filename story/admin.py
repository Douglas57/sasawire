from django.contrib import admin
from .models import StoryItem,Story
# Register your models here.
admin.site.register(StoryItem)
admin.site.register(Story)