from django.db import models
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from PIL import Image
from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
import random
import string

from django.utils.text import slugify


User = get_user_model()
def stories_videos_directory_path(instance, filename):
# file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'stories/videos/{0}/{1}'.format(instance.creator_name, filename)
def stories_photos_directory_path(instance, filename):
# file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'stories/photos/{0}/{1}'.format(instance.creator_name, filename)


def validate_file_size(value):
    filesize = value.size
    #1048576 bytes = 1MB
    if filesize > 10485760:
        raise ValidationError("The maximum file size that can be uploaded is 10MB")
class Story(models.Model):
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='owner_stories', related_query_name='owner_stories')
    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def story_items(self):
        return StoryItem.objects.filter(creator_id=self.owner_id);
    @property
    def owner_name(self):
        try:
            if self.owner.first_name:
                return f'{self.owner.first_name} {self.owner.last_name}'
            else:
                return f'{self.owner.username}'
        except:
            return 'Name not set'
    def __str__(self):
        return self.owner_name

class StoryItem(models.Model):
    """Model For Blog Posts"""

    creator = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='stories', related_query_name='stories')
    slug = models.SlugField(blank=True, null=True)
    image = models.ImageField(upload_to=stories_photos_directory_path, null=True,max_length=255,blank=True)
    video = models.FileField(upload_to=stories_videos_directory_path,null=True,max_length=255,
                             blank=True,
                             validators=[validate_file_size])
    link =models.URLField(null=True,blank=True)
    linkText=models.CharField(null=True,max_length=255,blank=True,default='Link')
    created_at = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        try:
            Story.objects.get_or_create(owner_id=self.creator_id)
        except:
            pass
        finally:
            super().save(*args, **kwargs)



    def __str__(self):
        return str(self.created_at)



    @property
    def creator_name(self):
        try:

            return f'{self.creator.username}'
        except:
            return 'Name not set'

    class Meta:
        indexes = [models.Index(fields=['slug'])]
        ordering = ['-created_at']


@receiver(post_save, sender=StoryItem)
def generate_unique_slug_for_categories(sender, instance, created, *args, **kwargs):

    if created:
        instance.slug = unique_slug_generator(instance)
        instance.save()


def random_string_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def unique_slug_generator(instance, new_slug=None):
    """
    This is for a Django project and it assumes your instance
    has a model with a slug field and a title character (char) field.
    """
    if new_slug is not None:
        slug = new_slug
    else:
        slug = slugify(str(instance.created_at))

    Klass = instance.__class__
    qs_exists = Klass.objects.filter(slug=slug).exists()
    if qs_exists:
        new_slug = "{slug}-{randstr}".format(
            slug=slug,
            randstr=random_string_generator(size=4)
        )
        return unique_slug_generator(instance, new_slug=new_slug)
    return slug


