from django.db import models
from django.contrib.auth import get_user_model
from post.models import Post

User = get_user_model()

class Comment(models.Model):
    """Model For The Comments In The Blog Posts"""
    creator = models.ForeignKey(
        User, on_delete=models.SET(None), related_name='comments', related_query_name='comments', null=True)
    parent_comment = models.ForeignKey(
        "self", on_delete=models.SET(None), related_name='children', related_query_name='children', null=True,
        blank=True)
    body = models.TextField()
    post = models.ForeignKey(Post, on_delete=models.CASCADE,
                             related_name='comments', related_query_name='comment')
    is_displayed = models.BooleanField(default=True)
    published_on = models.DateTimeField(auto_now_add=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,null=True)


    def __str__(self):
        return f'Post - "{self.post.title}", Body - "{self.body}"'

    @property
    def creator_name(self):
        try:
            if self.creator.first_name:
                return f'{self.creator.first_name} {self.creator.last_name}'
            else:
                return f'{self.creator.username}'
        except:
            return 'Name not set'

    @property
    def children(self):
        return self.children.objects.all()

    class Meta:

        ordering = ['-created_at']
