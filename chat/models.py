from django.contrib.auth import get_user_model
from django.db import models
from user_profile.models import UserProfile

User = get_user_model()

def chats_directory_path(instance, filename):

    return 'chats/{0}/'.format(instance.id)
class Contact(models.Model):
    user = models.ForeignKey(
        User, related_name='friends', on_delete=models.CASCADE)
    friends = models.ManyToManyField('self', blank=True)

    def __str__(self):
        return self.user.username


class Message(models.Model):
    contact = models.ForeignKey(
        UserProfile, related_name='messages', on_delete=models.CASCADE)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.contact.user.username


class Chat(models.Model):

    name=models.CharField(max_length=255,unique=True,default='self.id')
    image = models.ImageField(default='chats/default.jpg', upload_to=chats_directory_path, null=True)
    description = models.TextField(null=True,blank=True)
    is_group_chat=models.BooleanField(default=False)
    participants = models.ManyToManyField(
        UserProfile, related_name='chats', blank=True)
    messages = models.ManyToManyField(Message, blank=True)

    def __str__(self):
        return "{}".format(self.pk)


