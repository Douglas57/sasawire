from rest_framework import serializers

from comment.models import Comment


class CommentSerializer(serializers.ModelSerializer):
    """DRF Serializer For Listing Published Comment"""

    def get_fields(self):
        fields=super(CommentSerializer,self).get_fields()
        fields['children']=CommentSerializer(many=True)
        fields['creator_name']=serializers.CharField()
        return fields
    class Meta:
        model = Comment
        fields = [ 'id','body', 'published_on','creator','creator_name','created_at']


class CommentCreateSerializer(serializers.ModelSerializer):
    """DRF Serializer Fpr Creating Comments By The User"""

    class Meta:
        model = Comment
        fields = ['id','creator', 'body', 'post','parent_comment']
