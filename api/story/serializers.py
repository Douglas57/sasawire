from rest_framework import serializers

from story.models import StoryItem,Story
class StoryCreateSerializer(serializers.ModelSerializer):
    """DRF Serializer to create Story"""


    class Meta:
        model = StoryItem
        fields = ['creator', 'image', 'video', 'link', 'linkText']
class StoryItemsSerializer(serializers.ModelSerializer):
    """DRF Serializer to create Story"""

    class Meta:
        model = StoryItem
        fields = ['id','creator', 'image', 'video', 'link', 'linkText','created_at']


class StoryListSerializer(serializers.ModelSerializer):





    def get_fields(self):
        fields=super(StoryListSerializer,self).get_fields()
        fields['story_items']=StoryItemsSerializer(many=True)
        fields['owner_name']=serializers.CharField()
        return fields

    class Meta:
        model = Story
        fields = ['owner','owner_name','story_items']

