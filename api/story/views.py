from rest_framework import status, generics
from rest_framework.parsers import  FileUploadParser
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import StoryCreateSerializer,StoryListSerializer
from story.models import Story
class StoryCreateView(APIView):
    parser_class = (FileUploadParser,)

    def post(self, request, *args, **kwargs):
        print(request.user.pk)
        print(request.data)
        request.data['creator']  = request.user.pk
        # request.data['video']  = request.FILES['video']
        print(request.data,)
        try:

                storyitem_serializer = StoryCreateSerializer(data=request.data)
                if storyitem_serializer.is_valid():
                    print(storyitem_serializer)
                    storyitem_serializer.save()
                    return Response(storyitem_serializer.data, status=status.HTTP_201_CREATED)
                else:
                    print(storyitem_serializer.errors)
                    return Response(storyitem_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as exc:
                print(exc)
                return Response(storyitem_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class StoryListView(generics.ListAPIView):
    """View For List All Published Posts"""

    queryset = Story.objects.all()
    serializer_class = StoryListSerializer
    lookup_field = 'id'

