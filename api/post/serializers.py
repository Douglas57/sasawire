from rest_framework import serializers

from post.models import Post
from api.comment.serializers import CommentSerializer


class PostListSerializer(serializers.ModelSerializer):
    """DRF Serializer Listing All The Blog Posts"""

    total_comments = serializers.IntegerField()
    author_full_name = serializers.CharField()
    category_title=serializers.CharField()

    class Meta:
        model = Post
        fields = ['id','slug', 'title','category_slug', 'category','category_title','short_description','is_commentable',
                  'total_comments', 'author_full_name', 'published_on','body','image','created_on','last_edited']


class PostDetailSerializer(serializers.ModelSerializer):
    """DRF Serializer For Details Of The Blog Posts"""

    total_comments = serializers.IntegerField()
    author_full_name = serializers.CharField()
    category_title = serializers.CharField()

    class Meta:
        model = Post
        fields = ['slug', 'title', 'category','category_slug', 'category_title', 'short_description', 'is_commentable',
                  'total_comments', 'author_full_name', 'published_on', 'body', 'image', 'created_on', 'last_edited']
