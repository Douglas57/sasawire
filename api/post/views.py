from rest_framework import generics 
from rest_framework.response import Response
from .serializers import PostListSerializer, PostDetailSerializer
from post.models import Post
from rest_framework.decorators import api_view

class PostListView(generics.ListAPIView):
    """View For List All Published Posts"""

    queryset = Post.objects.filter(is_published=True)
    serializer_class = PostListSerializer
    lookup_field = 'slug'
class PostByTypeListView(generics.ListAPIView):
    """View For List All Published Posts By Type"""


    def get_queryset(self):
        return Post.objects.filter(is_published=True,post_tye_id=self.kwargs['post_type'])
    serializer_class = PostListSerializer

class PostByCategoryListView(generics.ListAPIView):
    """View For List All Published Posts By Type"""


    serializer_class = PostListSerializer
    def get_queryset(self):
        return Post.objects.filter(is_published=True,category__slug=self.kwargs['category'])


@api_view(('GET',))
def get_posts_by_type(request, post_type):
    posts=Post.objects.filter(is_published=True,post_tye_id=post_type)

    if posts != None:

        serializer = PostListSerializer(posts,many=True)
        return Response(serializer.data, status=200)
    else:
        return Response(status=404, )
@api_view(('GET',))
def get_nav_posts(request, post_type):

    posts=Post.objects.filter(is_published=True,post_tye_id=post_type)

    if posts != None:

        serializer = PostListSerializer(posts,many=True)
        return Response(serializer.data, status=200)
    else:
        return Response(status=404, )

@api_view(('GET',))
def get_breaking_international(request):


        breaking = Post.objects.filter(is_published=True,is_breaking=True)
        international=Post.objects.filter(is_published=True,is_international=True)

        if breaking!= None or international !=None:

            breaking_serializer = PostListSerializer(breaking, many=True)
            international_serializer = PostListSerializer(breaking, many=True)
            return Response(status=200,data={'international':international_serializer.data,
                                             'breaking':breaking_serializer.data} )
        else:
            return Response(status=404, )
@api_view(('GET',))
def get_posts_by_category(request, category):
    posts=Post.objects.filter(is_published=True,category__slug=category)

    if posts:
        serializer = PostListSerializer(posts)
        return Response(serializer.data, status=200)
    else:
        return Response(status=404, )


class PostDetailView(generics.RetrieveAPIView):
    """View For The Details Of A Single Post"""

    queryset = Post.objects.all()
    serializer_class = PostDetailSerializer
    lookup_field = 'slug'