from django.urls import path

from . import views

urlpatterns = [
    path('', views.PostListView.as_view()),
    path('view/<slug>/', views.PostDetailView.as_view(), name='post-detail'),
    path('type/<int:post_type>/', views.PostByTypeListView.as_view(), name='get_posts_by_type'),
    path('nav/type/', views.get_breaking_international, name='get_posts_nav_type'),
    path('category/<category>/', views.PostByCategoryListView.as_view(), name='get_posts_by_category'),
]