from django.urls import path

from . import views

urlpatterns = [
    path('', views.CategoryListView.as_view()),
    path('view/<slug>/', views.CategoryDetailView.as_view(), name='category-detail'),
]