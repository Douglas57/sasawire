from rest_framework import generics 

from .serializers import CategorytListSerializer
from category.models import Category
class CategoryListView(generics.ListAPIView):
    """View For List All Published Posts"""

    queryset = Category.objects.filter(parent_category=None)
    serializer_class = CategorytListSerializer
    lookup_field = 'slug'


class CategoryDetailView(generics.RetrieveAPIView):
    """View For The Details Of A Single Post"""

    queryset = Category.objects.all()
    serializer_class = CategorytListSerializer
    lookup_field = 'slug'