from rest_framework import serializers

from category.models import Category


class CategoryCoytListSerializer(serializers.ModelSerializer):
    """DRF Serializer Listing All The Categories"""

    creator_name=serializers.CharField()

    class Meta:
        model = Category
        fields = ['id','slug', 'title', 'description',
                  'image', 'creator', 'creator_name','parent_category','image','created_at','updated_at','children']

class CategorytListSerializer(serializers.ModelSerializer):





    def get_fields(self):
        fields=super(CategorytListSerializer,self).get_fields()
        fields['children']=CategorytListSerializer(many=True)
        fields['creator_name']=serializers.CharField()
        return fields

    class Meta:
        model = Category
        fields = ['id','slug', 'title', 'description',
                  'image', 'creator', 'creator_name','parent_category','image','created_at','updated_at','children']



