from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from rest_framework.documentation import include_docs_urls
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [

    path('api-auth/', include('rest_framework.urls')),
    path('chat/', include('chat.api.urls', namespace='chat')),
    path('rest-auth/', include('rest_auth.urls')),
    path('api-v1/rest-auth/registration/', include('api.registration.urls')),
    path('api-v1/categories/', include('api.category.urls')),
    path('api-v1/posts/', include('api.post.urls')),
    path('api-v1/admin-panel/', include('api.admin.urls')),
    path('api-v1/dashboard/', include('api.dashboard.urls')),
    path('api-v1/comments/', include('api.comment.urls')),
    path('api-v1/stories/', include('api.story.urls')),
    path('docs/', include_docs_urls(title='Blog API Documentation')),
    path(r'', TemplateView.as_view(template_name='index.html')),
    path('admin/', admin.site.urls)
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

