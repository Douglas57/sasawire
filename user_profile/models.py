from django.db import models
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver
from PIL import Image
User = get_user_model()
def profiles_directory_path(instance, filename):

    return 'profiles/{0}/'.format(instance.id)

class UserProfile(models.Model):
    """Model For Extending Default Django User Model"""

    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='profile')
    friends = models.ManyToManyField('self', blank=True)
    image = models.ImageField(default='profiles/default.jpg', upload_to=profiles_directory_path, null=True)
    website = models.URLField(blank=True, default="")
    bio = models.TextField(blank=True, max_length=100, default="")


    def __str__(self):
        return self.user.username

    @property
    def full_name(self):
        return f'{self.user.first_name} {self.user.last_name}'

    @property
    def username(self):
        return self.user.username

    def save(self, *args, **kwargs):
            super().save()

            img = Image.open(self.image.path)

            if img.height > 300 or img.width > 300:
                output_size = (300, 300)
                img.thumbnail(output_size)
                img.save(self.image.path)

    @receiver(post_save, sender=User)
    def create_profile(sender, instance, created, *args, **kwargs):
        """Automatically Create A User Profile When A New User IS Registered"""

        if created:
            user_profile = UserProfile(user=instance)
            user_profile.save()
